﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using RecordingPlayground.Properties;
using SeeStreamSharp.Media;
using SeeStreamSharp.RTSP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace RecordingPlayground
{
    public partial class Form1 : Form
    {
        private const string START = "Start";
        private const string STARTING = "Starting...";
        private const string STOP = "Stop";
        private const string STOPPING = "Stopping...";

        #region Logging support

        private static Form1 _this;

        internal static void AddLogMessage(string name, LogLevel level, object message, Exception exception)
        {
            if (_this == null)
                return;

            if (exception != null)
            {
                string msg = message.ToString();
                if (msg.Length > 0)
                    msg += Environment.NewLine;
                message = msg + exception.ToString();
            }

            //  Set the time already here as it may be asynchronously posted to the ListView control.
            _this.AddToLV(new LogItem(name, DateTime.Now.ToString("HH:mm:ss.fff"),
                level, message, Thread.CurrentThread.ManagedThreadId.ToString()));
        }

        private static readonly string[] crlfList = new string[] { Environment.NewLine };
        private class LogItem
        {
            public string Name;
            public string Time;
            public LogLevel Level;
            public object Message;
            public string Thread;

            public LogItem(string name, string time, LogLevel level, object message, string thread)
            {
                Name = name;
                Time = time;
                Level = level;
                Message = message;
                Thread = thread;
            }
        }

        #region ListView flicker workaround

        private const int WM_SETREDRAW = 0x000B;

        public static void Suspend(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        public static void Resume(Control control)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);

            control.Invalidate();
        }

        #endregion

        private void AddToLV(LogItem item)
        {
            if (InvokeRequired)
            {
                try
                {
                    //  This may throw exception below when the window is closed.
                    Invoke(new Action<LogItem>(AddToLV), new object[] { item });
                }
                catch (ObjectDisposedException)
                {
                }
                return;
            }

            string[] lines = item.Message.ToString().Split(crlfList, StringSplitOptions.RemoveEmptyEntries);

            //  NOT use BeginUpdate and EndUpdate, because these cause screen
            //  updates regardless of whether the update was on the current view or not.
            //listView1.BeginUpdate();
            Suspend(listView1);

            string[] subItems = new string[] {
                    item.Thread,
                    item.Level.ToString(),
                    item.Name,
                    lines[0]
                };
            listView1.Items.Add(item.Time).SubItems.AddRange(subItems);

            if (lines.Length > 1)
            {
                subItems[0] = string.Empty;
                subItems[1] = string.Empty;
                subItems[2] = string.Empty;

                for (int index = 1; index < lines.Length; index++)
                {
                    subItems[3] = lines[index];
                    listView1.Items.Add(item.Time).SubItems.AddRange(subItems);
                }
            }

            Resume(listView1);
            //listView1.EndUpdate();
        }

        #endregion

        private readonly ILog _logger = LogManager.GetLogger<Form1>();
        private bool _urlCommentsDirty;

        public Form1()
        {
            Presentation.Init();

            _this = this;

            InitializeComponent();
        }

        #region Form load and close

        private void Form1_Load(object sender, System.EventArgs e)
        {
            UrlCommentsList data = Settings.Default.UrlCommentsList;
            if (data != null)
            {
                foreach (UrlComment item in data.Data)
                {
                    Uri url;
                    if (Uri.TryCreate(item.OriginalString, UriKind.Absolute, out url))
                    {
                        int row = dataGridView1.Rows.Add();
                        DataGridViewCellCollection cells = dataGridView1.Rows[row].Cells;
                        DataGridViewCell urlCell = cells[Column1.Index];
                        //  See comment in CellValidating()...
                        urlCell.Value = item.OriginalString;
                        urlCell.Tag = url;
                        cells[Column4.Index].Value = item.Comment;
                        cells[Column3.Index].Value = START;
                    }
                }
            }

            //  Reset it back to false after loading the grid.
            _urlCommentsDirty = false;

            //dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);

            //  Mono never calls DefaultValuesNeeded event. Using UserAddedRow event as
            //  workaround. Need to set the initial new row value already here.
            dataGridView1.Rows[dataGridView1.NewRowIndex].Cells[Column3.Index].Value = START;

            _logger.Info("Ready");
            toolStripStatusLabel1.Text = "Ready";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_urlCommentsDirty)
            {
                List<UrlComment> data = new List<UrlComment>(dataGridView1.Rows.Count);

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (!row.IsNewRow)
                    {
                        Uri url = row.Cells[Column1.Index].Tag as Uri;
                        string comment = row.Cells[Column4.Index].Value as string;
                        data.Add(new UrlComment() { OriginalString = url.OriginalString, Comment = comment });
                    }
                }

                UrlCommentsList usl = new UrlCommentsList();
                usl.Data = data.ToArray();
                Settings.Default.UrlCommentsList = usl;

                Settings.Default.Save();
            }
        }

        #endregion

        #region Editing and validating cell content

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //  Clear the row error in case the user presses ESC.   
            dataGridView1.Rows[e.RowIndex].ErrorText = null;
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == Column1.Index)
            {
                string original = e.FormattedValue as string;
                if (!string.IsNullOrEmpty(original))
                {
                    Uri url;
                    if (!Uri.TryCreate(original, UriKind.Absolute, out url))
                    {
                        dataGridView1.Rows[e.RowIndex].ErrorText = "Not an absolute Url";
                        e.Cancel = true;
                    }
                    else
                    {
                        //  .Net supports setting the cell Value property to the
                        //  validated result in CellValidating(). However, Mono
                        //  will overwrite the Value property after CellValidating()
                        //  has returned. Mono sets Value to the string object that
                        //  was the input to CellValidating()...
                        //  The workaround is to use the Tag property to store the
                        //  validated result.
                        var cell = dataGridView1.Rows[e.RowIndex].Cells[Column1.Index];
                        cell.Tag = url;
                    }
                }
                else
                {
                    //  If this is the user trying to leave the new row after pressing
                    //  Esc, let him do so!
                    if (!dataGridView1.Rows[e.RowIndex].IsNewRow)
                    {
                        dataGridView1.Rows[e.RowIndex].ErrorText = "Not allowed empty";
                        e.Cancel = true;
                    }
                }
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //  If Url or Comment is altered the content needs to be saved.
            if (e.ColumnIndex == Column1.Index || e.ColumnIndex == Column4.Index)
            {
                _urlCommentsDirty = true;
            }
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            //  The "replacement" new row should have Start set already when it is created.
            e.Row.Cells[Column3.Index].Value = START;
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            //  To enable save after deleting row(s).
            _urlCommentsDirty = true;
        }

        #endregion

        #region Stop and start recordings

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != dataGridView1.NewRowIndex)
            {
                DataGridViewButtonColumn bc = dataGridView1.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (bc != null && e.RowIndex >= 0)
                {
                    DataGridViewCell cell = dataGridView1.Rows[e.RowIndex].Cells[Column3.Index];
                    string text = cell.Value as string;
                    if (text.Equals(START))
                        StartRecording(cell);
                    if (text.Equals(STOP))
                        StopRecording(cell);
                }
            }
        }

        private async void StartRecording(DataGridViewCell buttonCell)
        {
            buttonCell.Value = STARTING;

            //  This will actually create the temp file.
            string absoluteFileName = Path.GetTempFileName();
            //  Delete the temp file again because the file extension will be replaced
            //  by the media container used to write the recording.
            File.Delete(absoluteFileName);

            DataGridViewRow row = dataGridView1.Rows[buttonCell.RowIndex];
            DataGridViewCell cell = row.Cells[Column1.Index];
            Uri url = cell.Tag as Uri;

            try
            {
                Presentation presentation = new Presentation(url);
                Recording recording = new Recording(presentation, absoluteFileName);

                await recording.StartAsync();

                buttonCell.Tag = recording;
                buttonCell.Value = STOP;

                DataGridViewCell fileCell = dataGridView1.Rows[buttonCell.RowIndex].Cells[Column2.Index];
                fileCell.Value = Path.GetFileName(recording.MediaContainer.FileName);
                fileCell.ToolTipText = recording.MediaContainer.FileName;
            }
            catch (AggregateException ae)
            {
                ae.Flatten().Handle(e => {
                    _logger.Error("Recording failed to start", e);
                    return true;
                });

                //  Reset the button to Start as this attempt has failed.
                buttonCell.Value = START;
            }
            catch (Exception ex)
            {
                _logger.Error("Early failure", ex);

                //  Reset the button to Start as this attempt has failed.
                buttonCell.Value = START;
            }
        }

        private async void StopRecording(DataGridViewCell buttonCell)
        {
            buttonCell.Value = STOPPING;

            Recording recording = buttonCell.Tag as Recording;
            //  Only attempt to stop it one time. Remove the recording from the cell
            //  already here.
            buttonCell.Tag = null;

            try
            {
                await recording.StopAsync();
            }
            catch (AggregateException ae)
            {
                ae.Flatten().Handle(e => {
                    _logger.Error("Recording failed to stop", e);
                    return true;
                });
            }

            buttonCell.Value = START;

            DataGridViewCell fileCell = dataGridView1.Rows[buttonCell.RowIndex].Cells[Column2.Index];
            fileCell.Value = null;
            fileCell.ToolTipText = null;
        }

        #endregion
    }

    [Serializable]
    public class UrlCommentsList
    {
        public UrlComment[] Data;
    }

    [Serializable]
    public class UrlComment
    {
        public string OriginalString;
        public string Comment;
    }
}
