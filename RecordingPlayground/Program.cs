﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using Common.Logging.Simple;
using System;
using System.Windows.Forms;

namespace RecordingPlayground
{
    /*
        If the following XML text is added to RecordingPlayground.exe.config file and adding
        the NuGet package for Common.Logging.Log4Net1215 to this project the log output will
        be written by log4net to the file name in the log4net configuration in the XML.

        <configuration>
            <configSections>
                <sectionGroup name="common">
                    <section name="logging" type="Common.Logging.ConfigurationSectionHandler, Common.Logging" requirePermission="false"/>
                </sectionGroup>
                <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" requirePermission="false"/>
            </configSections>
            <startup> 
                <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5"/>
            </startup>
            <common>
                <logging>
                    <factoryAdapter type="Common.Logging.Log4Net.Log4NetLoggerFactoryAdapter, Common.Logging.Log4Net1215">
                        <arg key="level" value="ALL" />
                        <arg key="configType" value="INLINE" />
                    </factoryAdapter>
                </logging>
            </common>
            <log4net>
              <appender name="FileAppender" type="log4net.Appender.FileAppender">
                <file value="${TMP}\log-file.txt" />
                <appendToFile value="true" />
                <encoding value="unicodeFFFE" />
                <layout type="log4net.Layout.PatternLayout">
                  <conversionPattern value="%date [%thread] %-5level %logger - %message%newline" />
                </layout>
              </appender>
              <root>
                <level value="DEBUG" />
                <appender-ref ref="FileAppender" />
              </root>
            </log4net>
        </configuration>
     */

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ILoggerFactoryAdapter startupAdapter = LogManager.Adapter;
            if (startupAdapter.GetType() == typeof(NoOpLoggerFactoryAdapter))
            {
                //  No other logger has been set up using an application configuration element.
                LogManager.Adapter = new LoggerFactoryAdapter();
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
