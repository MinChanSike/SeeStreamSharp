﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Mime;
using System.Text;

namespace SeeStreamSharp.RTSP
{
    internal class HeaderCollection : NameValueCollection
    {
        public const string Accept = "Accept";
        public const string WwwAuthenticate = "WWW-Authenticate";
        public const string Authorization = "Authorization";
        public const string ContentBase = "Content-Base";
        //public const string ContentEncoding = "Content-Encoding";
        public const string ContentLength = "Content-Length";
        public const string ContentType = "Content-Type";
        public const string CSeq = "CSeq";
        public const string Date = "Date";
        public const string Range = "Range";
        public const string RTPInfo = "RTP-Info";
        public const string Session = "Session";
        public const string Transport = "Transport";

        private static readonly char SP = ' ';
        private static readonly char HT = '\t';
        private static readonly char[] CommaSeparator = new char[] { ',' };
        private static readonly char[] SemiColonSeparator = new char[] { ';' };

        private static List<string> _semiColonValues = new List<string>()
        { RTPInfo, Session, Transport };

        private ContentType _contentType;

        public ContentType ContentTypeValue
        {
            get { return _contentType; }
            set { _contentType = value; Set(ContentType, _contentType.ToString()); }
        }

        private int _contentLength;

        public int ContentLengthValue
        {
            get { return _contentLength; }
            set { _contentLength = value; Set(ContentLength, _contentLength.ToString()); }
        }

        private int _cSeq;

        public int CSeqValue
        {
            get { return _cSeq; }
            set { _cSeq = value; Set(CSeq, _cSeq.ToString()); }
        }

        private string _lastHeaderName;

        public void ParseLine(string input)
        {
            int size = input.Length;
            int index = 0;

            //  First check if line has leading whitespaces (LWS).
            char ch = input[0];
            if (ch == SP || ch == HT)
            {
                while (index < size)
                {
                    ch = input[index];
                    if (ch == SP || ch == HT)
                    {
                        ++index;
                    }
                    else break;
                }
                if (index == size)
                {
                    //  An empty line?!?
                    return;
                }

                //  Leave _lastHeaderName untouched.
            }
            else
            {
                //  Get the header name.
                while (index < size)
                {
                    ch = input[index];
                    if (ch == ':')
                    {
                        _lastHeaderName = input.Substring(0, index);
                        //  Step over ':'.
                        ++index;
                        break;
                    }
                    ++index;
                }
                if (index == size)
                    throw new RtspException("Malformed header name");

                //  Pass any whitespace after the ':'.
                while (index < size)
                {
                    ch = input[index];
                    if (ch == SP || ch == HT)
                    {
                        ++index;
                    }
                    else break;
                }
                if (index == size)
                {
                    //  This is an empty header (no value) or possibly a multi-line
                    //  header having no value on the first line. In any case we are done.
                    return;
                }
            }

            //  The rest of the line is values to be added to the header name.
            //  Depending of header name it is a mulit-valued value built using comma or
            //  semi colon.

            //  First separate on comma.
            string[] values = input.Substring(index).Split(CommaSeparator, StringSplitOptions.None);

            //  If a semicolon header name split again but preserve a comma on the end.
            if (_semiColonValues.IndexOf(_lastHeaderName) != -1)
            {
                List<string> semiColonValues = new List<string>();
                for (int idx = 0; idx < values.Length; idx++)
                {
                    string item = values[idx];
                    string value = values.Length == 1 || idx == values.Length - 1 ? item : item + ",";

                    //  This split will result in zero strings in items if it is empty.
                    string[] items = value.Split(SemiColonSeparator, StringSplitOptions.RemoveEmptyEntries);
                    semiColonValues.AddRange(items);
                }
                values = semiColonValues.ToArray();
            }

            foreach (string value in values)
            {
                Add(_lastHeaderName, value.Trim());
            }

            //  This is potentially slightly inefficient but it avoids having to call a
            //  process method after all lines have been added.
            //  It also avoids comparing a string in an incompatible way to how it is done
            //  in the base class.
            if (CSeqValue == 0)
            {
                //  Get() is in the base class.
                string value = Get(CSeq);
                if (value != null)
                {
                    int i;
                    if (int.TryParse(value, out i))
                        _cSeq = i;
                }
            }
            if (ContentLengthValue == 0)
            {
                //  Get() is in the base class.
                string value = Get(ContentLength);
                if (value != null)
                {
                    int i;
                    if (int.TryParse(value, out i))
                        _contentLength = i;
                }
            }
        }

        public override string Get(int index)
        {
            string key = BaseGetKey(index);
            char separator = CommaSeparator[0];
            if (_semiColonValues.IndexOf(key) != -1)
            {
                separator = SemiColonSeparator[0];
            }

            ArrayList values = (ArrayList)BaseGet(index);
            StringBuilder sb = new StringBuilder(key);
            sb.Append(": ");

            AddAsOne(sb, values, separator);
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            ToString(sb);
            return sb.ToString();
        }

        public void ToString(StringBuilder sb)
        {
            for (int index = 0; index < Count; index++)
            {
                string key = BaseGetKey(index);
                char separator = CommaSeparator[0];
                if (_semiColonValues.IndexOf(key) != -1)
                {
                    separator = SemiColonSeparator[0];
                }
                sb.Append(key);
                sb.Append(": ");
                ArrayList values = (ArrayList)BaseGet(index);
                AddAsOne(sb, values, separator);
                sb.Append("\r\n");
            }
        }

        private void AddAsOne(StringBuilder sb, ArrayList values, char separator)
        {
            for (int i = 0; i < values.Count; i++)
            {
                if (i != 0)
                {
                    bool addSeparator = true;
                    if (separator == SemiColonSeparator[0])
                    {
                        if (sb[sb.Length - 1] == CommaSeparator[0])
                            addSeparator = false;
                    }
                    if (addSeparator)
                        sb.Append(separator);
                }
                sb.Append(values[i] as string);
            }
        }
    }
}
