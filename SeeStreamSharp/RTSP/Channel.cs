﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections.Generic;

namespace SeeStreamSharp.RTSP
{
    internal class Channel
    {
        private readonly IClientConnection _con;
        private readonly int _channel;
        private readonly IObserver<ArraySegment<byte>> _receiver;

        internal Channel(IClientConnection con, int channel, IObserver<ArraySegment<byte>> receiver)
        {
            if (con == null)
                throw new ArgumentNullException("con");
            if (receiver == null)
                throw new ArgumentNullException("receiver");

            _con = con;
            _channel = channel;
            _receiver = receiver;
        }

        public void Received(ArraySegment<byte> packet)
        {
            _receiver.OnNext(packet);
        }

        public void Error(Exception ex)
        {
            _receiver.OnError(ex);
        }

        public void Close()
        {
            _receiver.OnCompleted();
        }

        public void Send(IList<ArraySegment<byte>> data)
        {
            _con.SendInterleavedData(_channel, data);
        }
    }
}
