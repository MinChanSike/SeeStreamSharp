﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections.Specialized;

namespace SeeStreamSharp.RTSP
{
    /// <summary>
    /// Provides a relation between authentication information provided to a Presentation
    /// and needs to be available to requests when they are sent.
    /// </summary>
    internal class AuthenticationProvider
    {
        private static readonly char[] COLON_DELIMITER = new char[] { ':' };

        /// <summary>
        /// Create an authentication provider if the Url contains authentication information.
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public static AuthenticationProvider Create(Uri resource)
        {
            string userInfo = resource.UserInfo;
            if (!string.IsNullOrEmpty(userInfo))
            {
                string[] parts = userInfo.Split(COLON_DELIMITER, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 2)
                {
                    return new AuthenticationProvider(parts[0], parts[1]);
                }
            }

            return null;
        }

        private readonly string _username, _password;
        private readonly ListDictionary _connectionAuthorization = new ListDictionary(Presentation.SingletonReferenceEquality);

        private AuthenticationProvider(string username, string password)
        {
            _username = username;
            _password = password;
        }

        /// <summary>
        /// Process a response to get a possible authentication challenge.
        /// </summary>
        /// <param name="clientConnection"></param>
        /// <param name="response"></param>
        /// <returns>Resend using the extracted authentication challenge.</returns>
        internal bool Unauthorized(IClientConnection clientConnection, Response response, Request request)
        {
            DigestClient previousDc = _connectionAuthorization[clientConnection] as DigestClient;

            //  Assuming that an Unauthorized response received on a connection
            //  that already has a challenge is caused by invalid username and/or
            //  password and should result in an RTSP exception.
            bool shouldResend = previousDc == null;

            DigestClient dc = DigestClient.Parse(response);
            if (dc == null)
                //  Any previous challenge should be removed.
                _connectionAuthorization.Remove(clientConnection);
            else
            {
                dc.SetUserNameAndPassword(_username, _password);
                //  Store the request that triggered this challenge.
                dc._challenged = request;

                //  A Digest authentication challenge explicitly states a need
                //  to resend as a result of an updated challenge by including
                //  "stale=true". (Username and password is valid but e.g. nonce
                //  has expired.)
                //  Continuously responding with "stale=true" would cause a never
                //  ending loop of re-sends. If previousDc also was "stale=true",
                //  don't resend and let it end in an exception.
                //  What if the "stale=true" is actually received again but in
                //  another request after some time of streaming? This could
                //  happen to a separate keep-alive request some time in the
                //  future...
                //  The above loop-detection should only apply per request.
                if (previousDc != null &&
                    previousDc._challenged == request &&
                    !previousDc._stale)
                {
                    shouldResend |= dc._stale;
                }

                //  Use indexer to add or replace a current authenticator.
                _connectionAuthorization[clientConnection] = dc;
            }

            return shouldResend;
        }

        internal void AddAuthorization(Request request, IClientConnection clientConnection)
        {
            DigestClient dc = _connectionAuthorization[clientConnection] as DigestClient;
            if (dc != null)
            {
                dc.CreateAuthorizationValue(request);
            }
        }
    }
}
