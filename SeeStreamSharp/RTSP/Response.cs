﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Net;

namespace SeeStreamSharp.RTSP
{
    internal class Response : Message
    {
        public const string startOfLine = "RTSP/1.0 ";

        public const HttpStatusCode UnsupportedTransport = (HttpStatusCode)461;

        public static Response ParseResponseLine(string line)
        {
            if (line.StartsWith(startOfLine, StringComparison.Ordinal))
            {
                string status = line.Substring(startOfLine.Length, 3);
                int statusCode = 0;
                if (int.TryParse(status, out statusCode))
                {
                    //  The response status code should be a three digit number.
                    if (statusCode >= (int)HttpStatusCode.Continue && statusCode < 1000)
                    {
                        string reasonPhrase = line.Substring(13);
                        Response response = new Response((HttpStatusCode)statusCode, reasonPhrase);
                        return response;
                    }
                }
            }

            return null;
        }

        public HttpStatusCode StatusCode { get; private set; }
        public string ReasonPhrase { get; private set; }

        public Response(HttpStatusCode statusCode, string reasonPhrase)
            : base(Requests.RESPONSE)
        {
            StatusCode = statusCode;
            ReasonPhrase = reasonPhrase;
        }

        protected sealed override string FirstLineToString()
        {
            //  RTSP/1.0 200 OK
            return string.Format("RTSP/1.0 {0} {1}", (int)StatusCode, ReasonPhrase);
        }
    }
}
