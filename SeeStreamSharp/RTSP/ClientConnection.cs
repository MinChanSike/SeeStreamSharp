﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace SeeStreamSharp.RTSP
{
    internal abstract class ClientConnection : IClientConnection, IMessageReceiver, IDisposable
    {
        protected readonly ILog _logger = LogManager.GetLogger<ClientConnection>();
        protected readonly int _loggingId;

        private readonly string _scheme;
        private readonly IPAddress[] _addressList;
        private readonly int _port;
        private readonly ReceiveBuffer _receiveBuffer;
        private readonly SocketAsyncEventArgs _overlappedReceive = new SocketAsyncEventArgs();
        //  Probably there is at most only 2 simultaneous threads:
        //  Always a single asynch read worker and possibly external code adding/deleting
        //  channels.
        private readonly ConcurrentDictionary<int, Channel> _rtspChannels = new ConcurrentDictionary<int, Channel>(2, 31);
        private int _lastReservedChannelNumber;

        private Encoding _encoder = Encoding.UTF8;
        private int _lastCSeq;

        private struct TimeoutRequest
        {
            public Request Request;
            public Timer Timeout;

            public TimeoutRequest(Request request, Timer timeout)
            {
                Request = request;
                Timeout = timeout;
            }
        }

        //  To not corrupt this list a lock on "this" is required.
        //  It is OK to take a lock on "this", it is internal.
        private LinkedList<TimeoutRequest> _requests = new LinkedList<TimeoutRequest>();

        protected readonly Socket _s;

        public Uri Url { get; set; }

        public virtual bool Connected
        {
            get { return _s.Connected; }
        }

        protected ArraySegment<byte> Buffer
        {
            get { return _receiveBuffer.Buffer; }
        }

        protected ClientConnection(string scheme, IPAddress[] addressList, int port, Socket s)
        {
            _scheme = scheme;
            _addressList = addressList;
            _port = port;
            _s = s;

            _receiveBuffer = new ReceiveBuffer(this);
            _overlappedReceive.Completed += OverlappedReceive_Completed;

            //  When logging is disabled it should not cause any garbage (heap
            //  allocations) at all in the key code path.
            DoStartReceiveLogAction = new Action<FormatMessageHandler>(DoStartReceiveLog);
            OverlappedReceive_CompletedLogAction = new Action<FormatMessageHandler>(OverlappedReceive_CompletedLog);

            _loggingId = RuntimeHelpers.GetHashCode(this);
            _logger.DebugFormat("[{0}] created", _loggingId);
        }

        #region No heap allocation logging

        private readonly Action<FormatMessageHandler> DoStartReceiveLogAction;

        private ArraySegment<byte> _rb;

        private void DoStartReceiveLog(FormatMessageHandler fmh)
        {
            fmh("[{0}] DoStartReceive() offset {1}, count {2}", _loggingId, _rb.Offset, _rb.Count);
        }

        private readonly Action<FormatMessageHandler> OverlappedReceive_CompletedLogAction;

        private int _received;
        private SocketError _socketError;

        private void OverlappedReceive_CompletedLog(FormatMessageHandler fmh)
        {
            fmh("[{0}] OverlappedReceive_Completed() received {1}, socket error {2}", _loggingId, _received, _socketError);
        }

        #endregion

        internal bool IsCompatibleConnection(string scheme, IPAddress[] addressList, int port)
        {
            if (scheme.Equals(_scheme, StringComparison.OrdinalIgnoreCase))
            {
                if (port == _port)
                {
                    IPEndPoint ep = _s.RemoteEndPoint as IPEndPoint;
                    if (ep != null)
                    {
                        foreach (IPAddress addr in addressList)
                        {
                            if (ep.Address == addr)
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }

            return false;
        }

        internal void Connect()
        {
            DoConnect();

            DoStartReceive();
        }

        void IClientConnection.Send(Request request)
        {
            int cSeq = Interlocked.Increment(ref _lastCSeq);
            request.SetCSeq(cSeq);

            if (request.AuthenticationProvider != null)
                request.AuthenticationProvider.AddAuthorization(request, this);

            string cmd = request.ToString();

            _logger.DebugFormat("[{0}] Send() {1}", _loggingId, cmd);
            //  If this throws, assume that the connection is broken. However, no additional
            //  action is required. The failure will also be detected by the pending receive.
            DoSend(_encoder.GetBytes(cmd));

            //  Create an individual timer for each request/CSeq.
            //  The downside is additional GC allocations but it makes the probable timer
            //  cancelation - and the possible timeout processing - easier.
            Timer timer;
            using (AsyncFlowControl afc = ExecutionContext.SuppressFlow())
            {
                timer = new Timer(timeout_callback, cSeq, request.TimeOut, Timeout.Infinite);
            }

            lock (this)
            {
                _requests.AddLast(new TimeoutRequest(request, timer));
            }
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            int reservedChannelNumber = _lastReservedChannelNumber;

            //  This assumes that there is always going to be some unused even channel number...
            while (_lastReservedChannelNumber < 255)
            {
                if (_rtspChannels.TryAdd(reservedChannelNumber, null))
                    break;

                reservedChannelNumber = Interlocked.Add(ref _lastReservedChannelNumber, 2);
                if (reservedChannelNumber == 256)
                {
                    Interlocked.CompareExchange(ref _lastReservedChannelNumber, 0, 256);
                    reservedChannelNumber = 0;
                }
            }

            return reservedChannelNumber;
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Channel channel;
            if (_rtspChannels.TryRemove(reservedChannelNumber, out channel))
            {
                if (channel != null)
                    throw new InvalidOperationException("Reserved channel number had a channel object");
            }
            else
                throw new InvalidOperationException("Reserved channel number missing");
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            if (!_rtspChannels.TryAdd(channelNumber, new Channel(this, channelNumber, receiver)))
                throw new InvalidOperationException("Channel already used");
        }

        void IClientConnection.RemoveChannel(int channelNumber)
        {
            Channel channel;
            if (_rtspChannels.TryRemove(channelNumber, out channel))
            {
                if (channel == null)
                    throw new InvalidOperationException("Channel number does not have a channel object");

                channel.Close();
            }
            else
                throw new InvalidOperationException("Channel number missing");
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            int dataLength = 0;

            //  Not using foreach saves an enumerator object.
            for (int i = 0; i < list.Count; i++)
            {
                dataLength += list[i].Count;
            }

            Debug.Assert(dataLength < ushort.MaxValue, "Buffer overflow");

            byte[] header = new byte[4];
            header[0] = ReceiveBuffer.ASCII_DOLLAR;
            header[1] = (byte)channel;
            header[2] = (byte)(dataLength >> 8);
            header[3] = (byte)dataLength;

            list.Insert(0, new ArraySegment<byte>(header));

            _logger.DebugFormat("[{0}] SendInterleavedData() channel={1}, bytes={2}",
                _loggingId, channel, dataLength);
            //  If this throws, assume that the connection is broken. However, no additional
            //  action is required. The failure will also be detected by the pending receive.
            DoSend(list);
        }

        protected virtual void DoConnect()
        {
            _s.Connect(_addressList, _port);
            //  Now it is connected, set the socket to non-blocking.
            _s.Blocking = false;
        }

        //  When called from Connect() its allowed to throw on receive failure. An
        //  exception will then end up in the connection task.
        //  This is also called when the receive is finished and a new is to be posted.
        //  In this case an exception is also thrown and must then be handled in the
        //  calling method.
        protected virtual void DoStartReceive()
        {
            _rb = Buffer;
            _overlappedReceive.SetBuffer(_rb.Array, _rb.Offset, _rb.Count);
            _logger.Trace(DoStartReceiveLogAction);

            bool pending;
            using (AsyncFlowControl fc = ExecutionContext.SuppressFlow())
            {
                pending = _s.ReceiveAsync(_overlappedReceive);
            }

            if (!pending)
                //  This will cause recursive calls if it keeps ending synchrounosly...
                OverlappedReceive_Completed(_s, _overlappedReceive);
        }

        private void OverlappedReceive_Completed(object sender, SocketAsyncEventArgs e)
        {
            _received = e.BytesTransferred;
            _socketError = e.SocketError;
            _logger.Trace(OverlappedReceive_CompletedLogAction);

            if (e.SocketError != SocketError.Success)
            {
                //  If failed create a SocketException but not throw it. It would only go
                //  "down" the callstack to the worker thread pool. It needs to be sent "up"
                //  to all waiting requests and all open channels.
                AbortAll(new SocketException((int)e.SocketError));
                return;
            }

            //  Is the connection gracefully closed?
            if (_received == 0)
                return;

            Process(_received);
        }

        /// <summary>
        /// This is called to parse received bytes, and if not disposed, post the next
        /// read.
        /// </summary>
        /// <param name="read">Number of bytes added to the receive buffer.</param>
        protected void Process(int read)
        {
            try
            {
                _receiveBuffer.AddBytes(read);

                //  The above call to AddBytes() could trigger disposing of this object.
                //  However, that Dispose() call will be forced to run on the thread
                //  calling AddBytes() and eliminates the risk of a race condition!
                if (!_disposed)
                    DoStartReceive();
            }
            catch (Exception ex)
            {
                //  An exception has to be handled or it would only go "down" the
                //  callstack to the worker thread pool. It needs to be sent "up"
                //  to all waiting requests and all open channels.
                AbortAll(ex);
            }
        }

        protected virtual void DoSend(IList<ArraySegment<byte>> list)
        {
            //  IAsyncResult is discarded as it will be available in SendCallback().
            _s.BeginSend(list, SocketFlags.None, SendCallback, null);
        }

        protected virtual void DoSend(byte[] data)
        {
            //  IAsyncResult is discarded as it will be available in SendCallback().
            _s.BeginSend(data, 0, data.Length, SocketFlags.None, SendCallback, null);
        }

        private void SendCallback(IAsyncResult ar)
        {
            //  If an exception is thrown we have to catch it to prevent it from going "down"
            //  the callstack to the worker thread pool. All we can do is to send it "up" to
            //  all waiting requests and all open channels.
            try
            {
                int sent = _s.EndSend(ar);
            }
            catch (Exception ex)
            {
                AbortAll(ex);
            }
        }

        void IMessageReceiver.Process(Message message)
        {
            Response response = message as Response;
            Request request;

            if (response != null)
            {
                request = LockedRemoveNode(response.CSeq);

                if (request != null)
                {
                    try
                    {
                        //  If an exception from the RTSP protocol processing code is uncaught
                        //  it will eventually end up in OverlappedReceive_Completed()
                        //  terminating the connection. This would be fatal!
                        request.Process(response, null, this);
                    }
                    catch (Exception ex)
                    {
                        //  But there is no one to handle an exception so the only thing to
                        //  do is to log it.
                        _logger.ErrorFormat("[{0}] Process() calling Process(response) threw an exception",
                            ex, _loggingId);
                    }
                }
                else
                {
                    //  It is meaningless to throw an exception. It would go "down"
                    //  the callstack. Someone else to inform about the unexpected
                    //  response?!?
                    _logger.WarnFormat("[{0}] Process() unexpected/timed out CSeq {1} in response", _loggingId, response.CSeq);
                }

                return;
            }

            //  This is not a server but ANNOUNCE is supposed to be sent
            //  from server to client.
            request = message as Request;
            if (request != null)
                _logger.WarnFormat("[{0}] Process() unexpected REQUEST '{1}'", _loggingId, request.Request);
        }

        void IMessageReceiver.Process(int channelNumber, ArraySegment<byte> packet)
        {
            Channel channel;
            if (_rtspChannels.TryGetValue(channelNumber, out channel))
            {
                if (channel != null)
                {
                    try
                    {
                        //  This is the common code path of a call that is intended to end up
                        //  in external code outside of this assembly.
                        //  If an exception from external code is uncaught it will eventually
                        //  end up in OverlappedReceive_Completed() terminating the connection.
                        //  This would be fatal!
                        channel.Received(packet);
                    }
                    catch (Exception ex)
                    {
                        //  But there is no one to handle an exception so the only thing to
                        //  do is to log it.
                        _logger.ErrorFormat("[{0}] Process() calling Process(packet) threw an exception",
                            ex, _loggingId);
                    }
                }
                else
                    _logger.WarnFormat("[{0}] Process() interleaved packet received on un-initialized channel {1}", _loggingId, channelNumber);
            }
            else
                _logger.WarnFormat("[{0}] Process() interleaved packet received on UNEXPECTED channel {1}", _loggingId, channelNumber);
        }

        private void timeout_callback(object state)
        {
            Request request = LockedRemoveNode((int)state);

            //  If this CSeq was NOT in the list this timeout should be silently ignored.
            //  It may have been a race between the response and this timeout.
            if (request != null)
            {
                Exception thrown;

                try
                {
                    throw new RtspException(RtspException.TimeoutMessage);
                }
                catch (Exception ex)
                {
                    thrown = ex;
                }

                request.Process(null, thrown, this);
            }
        }

        private Request LockedRemoveNode(int cSeq)
        {
            Request request = null;

            lock (this)
            {
                LinkedListNode<TimeoutRequest> node = _requests.First;
                while (node != null)
                {
                    if (node.Value.Request.CSeq == cSeq)
                    {
                        node.Value.Timeout.Dispose();
                        request = node.Value.Request;
                        _requests.Remove(node);
                        break;
                    }

                    node = node.Next;
                }
            }

            return request;
        }

        protected void AbortAll(Exception ex)
        {
            //  First "abort" all requests...
            Request request = LockedRemoveFirst();
            while (request != null)
            {
                try
                {
                    request.Process(null, ex, this);
                }
                catch (Exception)
                {
                }
                request = LockedRemoveFirst();
            }

            //  ...and then send error to all interleaved channels.
            //  Also, remove all the channels to disable receiving.
            KeyValuePair<int, Channel>[] channels = _rtspChannels.ToArray();
            _rtspChannels.Clear();
            for (int i = 0; i < channels.Length; i++)
            {
                if (channels[i].Value != null)
                {
                    channels[i].Value.Error(ex);
                }
            }
        }

        private Request LockedRemoveFirst()
        {
            Request request = null;

            lock (this)
            {
                LinkedListNode<TimeoutRequest> node = _requests.First;
                if (node != null)
                {
                    request = node.Value.Request;
                    _requests.Remove(node);
                }
            }

            return request;
        }

        #region IDisposable Support

        //  To detect multiple calls and to stop the posting of new async receives in
        //  OverlappedReceive_Completed().
        private bool _disposed;

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                try
                {
                    if (disposing)
                    {
                        _overlappedReceive.Dispose();
                        _s.Shutdown(SocketShutdown.Both);
                        _s.Dispose();
                    }
                }
                finally
                {
                    _disposed = true;
                }
            }
        }

        #endregion
    }
}
