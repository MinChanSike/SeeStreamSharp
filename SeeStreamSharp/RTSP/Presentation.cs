﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Properties;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SeeStreamSharp.RTSP
{
    /// <summary>
    /// Provides a RTSP client that connects to a RTSP server.
    /// </summary>
    /// <remarks>
    /// Implements a presentation client using the RFC 2326 Real Time Streaming Protocol
    /// (RTSP, https://tools.ietf.org/html/rfc2326).
    /// <para>
    /// The operation of this network protocol is inherently asynchronous. To enable the
    /// external code to start an operation and have it run in the background, and then
    /// react to the result when it becomes available, all methods are asynchronous.
    /// </para>
    /// <para>
    /// This object is not re-usable. After constructing it you start with retreiving the
    /// session description and then move forward in sequence to setup the streams, start
    /// playing, and finaly stop playing. If at any step an error occurs or the object is
    /// no longer of use it can and should be disposed.
    /// </para>
    /// </remarks>
    public sealed class Presentation : ISessionDescriptionSetup, IAsyncStreamingClient
    {
        /// <summary>
        /// The URL scheme name for the RTSP protocol.
        /// </summary>
        public const string UriSchemeRtsp = "rtsp";

        internal const string UriSchemeRtspu = "rtspu";

        /// <summary>
        /// The URL scheme name for the SSL/TLS protected RTSP protocol.
        /// </summary>
        public const string UriSchemeRtsps = "rtsps";

        /// <summary>
        /// The default port for the RTSP server.
        /// </summary>
        public const int SchemeRtspPort = 554;
        /// <summary>
        /// The proposed default port for the RTSP version 2 SSL/TLS protected server.
        /// </summary>
        public const int SchemeRtspsPort = 322;

        /// <summary>
        /// Supported transport protocol.
        /// </summary>
        private const string TransportProtocol = "RTP/AVP";

        private const string InterleavedTransportPostfix = "/TCP";

        private const string TimeOutEqual = "timeout=";

        internal class ReferenceEquality : IComparer
        {
            public int Compare(object x, object y)
            {
                return ReferenceEquals(x, y) ? 0 : 1;
            }
        }

        internal static readonly ReferenceEquality SingletonReferenceEquality = new ReferenceEquality();

        /// <summary>
        /// The static constructor used to register the scheme and ports for the URI parser.
        /// </summary>
        /// <remarks>
        /// This propably should be in a module initializer. However, not even the module
        /// initializer is called until a type in the assembly is accessed. This may
        /// already be to late as the external code could already have constructed an URL.
        /// </remarks>
        static Presentation()
        {
            if (!UriParser.IsKnownScheme(UriSchemeRtsp))
                UriParser.Register(new HttpStyleUriParser(), UriSchemeRtsp, SchemeRtspPort);
            //if (!UriParser.IsKnownScheme(SchemeRtspu))
            //    UriParser.Register(new HttpStyleUriParser(), SchemeRtspu, SchemeRtspPort);
            if (!UriParser.IsKnownScheme(UriSchemeRtsps))
                UriParser.Register(new HttpStyleUriParser(), UriSchemeRtsps, SchemeRtspsPort);
        }

        private readonly ILog _logger = LogManager.GetLogger<Presentation>();
        private readonly int _loggingId;

        private readonly Uri _url;
        private Uri _baseUrl;
        private TaskCompletionSource<SessionDescription> _sdpTCS;
        private TaskCompletionSource<string> _playTCS;
        private TaskCompletionSource<string> _stopTCS;
        private int _timeout = Settings.Default.RtspCommandTimeout;
        private Exception _exception;

        /// <summary>
        /// key = this representing Describe, value = <see cref="Task{IClientConnection}"/>
        /// key = Media, value = <see cref="StreamSetup"/>
        /// </summary>
        private readonly ListDictionary _streams = new ListDictionary(SingletonReferenceEquality);

        /// <summary>
        /// The linked list of RTSP sessions and theire cookies.
        /// </summary>
        private RtspSession _sessions;

        /// <summary>
        /// AuthenticationProvider is created if a Url contains authentication information. The
        /// authentication information is not secured in memory because it is extracet from an
        /// unsecure source.
        /// </summary>
        private AuthenticationProvider _authenticationProvider;

        /// <summary>
        /// Gets the transport protocol names supported by this class.
        /// </summary>
        public string SupportedTransportProtocol
        {
            get { return TransportProtocol; }
        }

        /// <summary>
        /// Gets or sets the RTSP command timeout value.
        /// </summary>
        /// <value>
        /// The command timeout value in milliseconds.
        /// </value>
        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        /// <summary>
        /// Gets the presentation URL.
        /// </summary>
        /// <value>
        /// The <see cref="Uri"/> provided to create this object.
        /// </value>
        public Uri Url { get { return _url; } }

        /// <summary>
        /// Gets the RFC 2327 Session Description associated with this presentation.
        /// </summary>
        /// <value>
        /// The <see cref="SessionDescription"/> returned from the RTSP server.
        /// <para>
        /// Optionally it can be an out-of-band description used to construct this object.
        /// In this case it will not be retreived from the server. The provided description
        /// will be used to determine what media streans are available and how to set them
        /// up.
        /// </para>
        /// </value>
        public SessionDescription SessionDescription { get; private set; }

        /// <summary>
        /// Returns an exception if the presentation has failed.
        /// </summary>
        public Exception Exception { get { return _exception; } }

        /// <summary>
        /// Enable external code to fire the static constructor.
        /// </summary>
        /// <remarks>
        /// Calling this will not be necessary if f.x. using the static string <see cref="UriSchemeRtsp"/> 
        /// scheme name provided to build an URL before constructing the first <see cref="Presentation"/>.
        /// </remarks>
        public static void Init()
        {
        }

        /// <summary>
        /// Creates a presentation using the provided <see cref="Uri"/>.
        /// </summary>
        /// <param name="url">An RTSP presentation Url in absolute form.</param>
        public Presentation(Uri url)
        {
            if (url == null)
                throw new ArgumentNullException("url");
            if (!url.IsAbsoluteUri)
                throw new ArgumentException("Must be an absolute Url", "url");
            ClientConnectionFactory.CheckIfImplemented(url.Scheme);

            _url = url;
            _loggingId = _url.GetHashCode();

            //  Determine if authentication information is provided in the Url.
            _authenticationProvider = AuthenticationProvider.Create(url);

            _logger.InfoFormat("[{0}] created, url={1}", _loggingId, _url.ToString());
        }

        /// <summary>
        /// Creates a presentation using the provided <see cref="SessionDescription"/>.
        /// </summary>
        /// <param name="sd">An RTSP presentation <see cref="SessionDescription"/></param>
        /// <param name="baseUrl">Optional absolute <see cref="Uri"/> to use as base if <paramref name="sd"/> control attributes are relative.</param>
        public Presentation(SessionDescription sd, Uri baseUrl = null)
        {
            if (sd == null)
                throw new ArgumentNullException("sdp");
            if (baseUrl != null && !baseUrl.IsAbsoluteUri)
                throw new ArgumentException("Must be an absolute Url", "baseUrl");

            ControlAttribute control;

            //  If a baseUrl is not supplied then check SD for a control attribute having an absolute Url.
            if (baseUrl == null)
            {
                control = sd.SessionLevelTypeValues().OfType<ControlAttribute>().FirstOrDefault();
                if (control != null)
                {
                    if (control.Url != null)
                    {
                        if (!control.Url.IsAbsoluteUri)
                            throw new ArgumentException("SessionDescription control attribute does not contain an absolute Url", "sd");

                        ClientConnectionFactory.CheckIfImplemented(control.Url.Scheme);
                        baseUrl = control.Url;
                    }
                }
            }

            //  If we got this far without finding an absolute Url check if all media descriptions have one.
            if (baseUrl == null)
            {
                bool commonUrl = true;
                Uri schemeAndServer = null;

                foreach (Media mediaStream in sd.MediaDescriptions)
                {
                    control = mediaStream.OfType<ControlAttribute>().FirstOrDefault();
                    if (control != null)
                    {
                        if (control.Url != null)
                        {
                            if (control.Url.IsAbsoluteUri)
                            {
                                ClientConnectionFactory.CheckIfImplemented(control.Url.Scheme);

                                if (schemeAndServer == null)
                                    schemeAndServer = new Uri(control.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped),
                                                              UriKind.Absolute);
                                else
                                {
                                    int eq = Uri.Compare(schemeAndServer, control.Url, UriComponents.SchemeAndServer,
                                                         UriFormat.Unescaped, StringComparison.OrdinalIgnoreCase);
                                    if (eq != 0)
                                        commonUrl = false;
                                }
                                continue;
                            }
                        }
                    }

                    throw new ArgumentException("SessionDescription media stream control attribute does not contain an absolute Url", "sd");
                }

                if (commonUrl)
                    baseUrl = schemeAndServer;
            }

            //  A fixed SD does not require the target to be queryied.
            _sdpTCS = new TaskCompletionSource<SessionDescription>();
            _sdpTCS.SetResult(sd);

            SessionDescription = sd;
            _baseUrl = baseUrl;
            _loggingId = _baseUrl.GetHashCode();

            _logger.InfoFormat("[{0}] created, baseUrl={1}", _loggingId, _baseUrl.ToString());
        }

        /// <summary>
        /// Start a session description request.
        /// </summary>
        /// <remarks>
        /// This will also initialize the transport connection to the RTSP server.
        /// </remarks>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        public Task<SessionDescription> GetSessionDescriptionAsync()
        {
            _logger.TraceFormat("[{0}] GetSessionDescriptionAsync() called", _loggingId);

            if (_playTCS != null)
                throw new InvalidOperationException("Streaming already started");

            //  It can never be faulted if constructed with a fixed SD.
            if (_sdpTCS == null || _sdpTCS.Task.IsFaulted)
            {
                lock (_streams.SyncRoot)
                {
                    if (_sdpTCS == null || _sdpTCS.Task.IsFaulted)
                    {
                        //  This is the only place where the Presentation exception can be reset.
                        _exception = null;
                        _sdpTCS = new TaskCompletionSource<SessionDescription>();

                        Task<IClientConnection> connTask = ClientConnectionFactory.GetConnectionAsync(_url);
                        connTask.ContinueWith(SendDescribeContinuation);
                        //  Can't use Add() as it may be a re-try.
                        _streams[this] = connTask;
                    }
                }
            }

            _logger.TraceFormat("[{0}] GetSessionDescriptionAsync() returning", _loggingId);
            return _sdpTCS.Task;
        }

        /// <summary>
        /// Task continuation to send a DESCRIBE request.
        /// </summary>
        /// <remarks>
        /// This method (task body) may not throw any exception. If it did there is
        /// now an "abandoned" task in faulted state and it will never be checked
        /// if it has faulted.
        /// <para>
        /// Any exception in this method has to be set on the task already given
        /// to the external code.
        /// </para>
        /// </remarks>
        /// <param name="connectionTask">Parent connection task.</param>
        private void SendDescribeContinuation(Task<IClientConnection> connectionTask)
        {
            _logger.TraceFormat("[{0}] SendDescribeContinuation() entered", _loggingId);

            try
            {
                //  This will trigger an exception if connection task is faulted.
                IClientConnection c = connectionTask.Result;
                Describe describe = new Describe(_url, ProcessDescribeResponse, _timeout)
                                                { AuthenticationProvider = _authenticationProvider };
                c.Send(describe);
            }
            catch (Exception ex)
            {
                Interlocked.CompareExchange(ref _exception, ex, null);
                _sdpTCS.SetException(_exception);
                _logger.ErrorFormat("[{0}] SendDescribeContinuation() set exception", _loggingId);
            }

            _logger.TraceFormat("[{0}] SendDescribeContinuation() exiting", _loggingId);
        }

        /// <summary>
        /// Delegate to run when receiving the DESCRIBE response.
        /// </summary>
        /// <remarks>
        /// This method may not throw an exception because it is called from "lower level"
        /// networking code. Any exception is to be set on the TaskCompletionSource used
        /// to give a Task to the external code.
        /// </remarks>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <param name="exception"></param>
        private void ProcessDescribeResponse(Request request, Response response, Exception exception)
        {
            _logger.TraceFormat("[{0}] ProcessDescribeResponse() entered", _loggingId);

            if (exception != null)
            {
                Interlocked.CompareExchange(ref _exception, exception, null);
                _sdpTCS.SetException(_exception);
                _logger.ErrorFormat("[{0}] ProcessDescribeResponse() exiting exception", exception, _loggingId);
                return;
            }

            try
            {
                Describe describe = request as Describe;
                SessionDescription = describe.Process(response);

                string[] values = response.Headers.GetValues(HeaderCollection.ContentBase);
                if (values != null)
                {
                    string baseUrl = values[0];
                    //  This code assumes that any Uri path separator needs to be included when adding parts.
                    if (baseUrl.EndsWith("/"))
                        baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
                    //  This is expected to throw an exception if it is not absolute.
                    _baseUrl = new Uri(baseUrl, UriKind.Absolute);
                }

                _sdpTCS.SetResult(SessionDescription);
            }
            catch (Exception ex)
            {
                Interlocked.CompareExchange(ref _exception, ex, null);
                _sdpTCS.SetException(_exception);
                _logger.ErrorFormat("[{0}] ProcessDescribeResponse() set exception", exception, _loggingId);
            }

            _logger.TraceFormat("[{0}] ProcessDescribeResponse() exiting", _loggingId);
        }

        /// <summary>
        /// Start a request to setup the transport mechanism to be used for a media stream.
        /// </summary>
        /// <param name="mediaValue">The <see cref="Media"/> object or an <see cref="TypeValue"/>
        /// object that has it as its <see cref="TypeValue.MediaParent"/>.</param>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        /// <exception cref="InvalidOperationException">
        /// The <see cref="SessionDescription"/> property is null.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// The <paramref name="mediaValue"/> parameter is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// The <paramref name="mediaValue"/> parameter is not a <see cref="Media"/> type
        /// or does not belong to the current <see cref="SessionDescription"/>.
        /// </exception>
        public Task<Session> SetupAsync(TypeValue mediaValue)
        {
            _logger.TraceFormat("[{0}] SetupAsync() called", _loggingId);

            if (SessionDescription == null)
                throw new InvalidOperationException("SessionDescription is null");
            if (mediaValue == null)
                throw new ArgumentNullException("mediaValue");
            if (_playTCS != null)
                throw new InvalidOperationException("Streaming already started");
            Media media = mediaValue as Media;
            media = media ?? mediaValue.MediaParent;
            if (media == null)
                throw new ArgumentException("Not an SdpMedia object or no such parent", "mediaValue");
            if (media.Session != SessionDescription)
                throw new ArgumentException("Not a media value in the current session descriptor", "mediaValue");

            StreamSetup streamSetup;
            lock (_streams.SyncRoot)
            {
                streamSetup = _streams[media] as StreamSetup;
                if (streamSetup != null)
                {
                    _logger.TraceFormat("[{0}] SetupAsync() returning in lock", _loggingId);
                    return streamSetup.Tcs.Task;
                }

                //  Build the setup URL.
                streamSetup = new StreamSetup(this, BuildSetupUrl(media));
                _streams.Add(media, streamSetup);
            }

            //  From now on any exception should be caught and set on the task returned
            //  to the caller...
            try
            {
                Task<IClientConnection> connectionTask = null;

                //  Check if a connection matching the scheme and server of the Url has
                //  already been used to submit a Setup. Then we have to wait for the
                //  completion to get the session cookie before we can submit this Setup
                //  request.
                foreach (DictionaryEntry de in _streams)
                {
                    Media deMedia = de.Key as Media;
                    if (deMedia != null)
                    {
                        //  Skip "myself" already added above.
                        if (!ReferenceEquals(media, deMedia))
                        {
                            StreamSetup deSetup = de.Value as StreamSetup;
                            if (Uri.Compare(streamSetup._setup.Url, deSetup._setup.Url,
                                            UriComponents.SchemeAndServer, UriFormat.Unescaped,
                                            StringComparison.OrdinalIgnoreCase)
                                == 0)
                            {
                                //  If this task is not completed the connection session
                                //  cookie is not yet available.
                                //  In any case a Task<IClientConnection> is required and
                                //  the easiest way to create one is a continuation.
                                connectionTask = deSetup.Tcs.Task.ContinueWith<IClientConnection>((sessionTask) =>
                                {
                                    //  Propagate any exception.
                                    sessionTask.Wait();

                                    return deSetup._setup.Connection;
                                });

                                break;
                            }
                        }
                    }
                }

                //  If no already used connectionTask was available the Describe request
                //  connection could possibly be used.
                if (connectionTask == null)
                {
                    if (Uri.Compare(_url, streamSetup._setup.Url,
                                    UriComponents.SchemeAndServer, UriFormat.Unescaped,
                                    StringComparison.OrdinalIgnoreCase) == 0)
                        connectionTask = _streams[this] as Task<IClientConnection>;
                }

                //  Last option is that a new connection has to be created.
                if (connectionTask == null)
                {
                    _logger.DebugFormat("[{0}] SetupAsync() create connection for stream setup", _loggingId);
                    //  Create an alternate Task<Connection> to send the Setup on.
                    connectionTask = ClientConnectionFactory.GetConnectionAsync(streamSetup._setup.Url);
                }

                connectionTask.ContinueWith(streamSetup.ConnectionContinuation);
            }
            catch (Exception ex)
            {
                Interlocked.CompareExchange(ref _exception, ex, null);
                streamSetup.Tcs.SetException(_exception);
                _logger.ErrorFormat("[{0}] SetupAsync() set exception completion", ex, _loggingId);
            }

            _logger.TraceFormat("[{0}] SetupAsync() returning", _loggingId);
            return streamSetup.Tcs.Task;
        }

        /// <summary>
        /// RTSP Setup sending and response processing.
        /// </summary>
        /// <remarks>
        /// This object exposes internal implementation details but this is acceptable as
        /// it is a private nested class.
        /// </remarks>
        private class StreamSetup
        {
            private readonly Presentation _presentation;
            public readonly Setup _setup;
            public readonly TaskCompletionSource<Session> Tcs = new TaskCompletionSource<Session>();

            public StreamSetup(Presentation presentation, Uri url)
            {
                _presentation = presentation;
                _setup = new Setup(url, ProcessSetupResponse, presentation._timeout)
                                  { AuthenticationProvider = presentation._authenticationProvider };
            }

            public void ConnectionContinuation(Task<IClientConnection> task)
            {
                try
                {
                    //  This will trigger an exception if connection task is faulted.
                    IClientConnection c = task.Result;

                    //  Now that the connection is available it is possible to allocate channels.
                    _setup.SetInterleavedTcpTransport(TransportProtocol + InterleavedTransportPostfix, c);

                    RtspSession session = RtspSession.GetSession(_presentation._sessions, c);
                    if (session != null && !string.IsNullOrEmpty(session._sessionCookie))
                        _setup.Headers.Add(HeaderCollection.Session, session._sessionCookie);

                    c.Send(_setup);
                }
                catch (Exception ex)
                {
                    Interlocked.CompareExchange(ref _presentation._exception, ex, null);
                    Tcs.SetException(_presentation._exception);
                    _presentation._logger.ErrorFormat("[{0}] StreamSetup.ConnectionContinuation() exception completion",
                        ex, _presentation._loggingId);
                }
            }

            /// <summary>
            /// Delegate to run when receiving the SETUP response.
            /// </summary>
            /// <remarks>
            /// This method may not throw an exception because it is called from "lower level"
            /// networking code. Any exception is to be set on the TaskCompletionSource used
            /// to give a Task to the external code.
            /// </remarks>
            /// <param name="request"></param>
            /// <param name="response"></param>
            /// <param name="exception"></param>
            private void ProcessSetupResponse(Request request, Response response, Exception exception)
            {
                _presentation._logger.TraceFormat("[{0}] StreamSetup.ProcessSetupResponse() entered", _presentation._loggingId);

                if (exception != null)
                {
                    Interlocked.CompareExchange(ref _presentation._exception, exception, null);
                    Tcs.SetException(_presentation._exception);
                    _presentation._logger.ErrorFormat("[{0}] StreamSetup.ProcessSetupResponse() exiting set exception",
                        exception, _presentation._loggingId);
                    return;
                }

                try
                {
                    //  Re-using SessionDescription as the lock object...
                    lock (_presentation.SessionDescription)
                    {
                        RtspSession session = RtspSession.GetSession(_presentation._sessions, _setup.Connection);
                        if (session == null)
                        {
                            string[] sessionCookie = response.Headers.GetValues(HeaderCollection.Session);
                            //  In the response of SETUP the following header should be returned.
                            //  Session: ABCDF;timeout=60
                            //  However, not all servers return a timeout value. Then assume 60 seconds.
                            if (sessionCookie != null)
                            {
                                int timeOut = 60;
                                if (sessionCookie.Length > 1 && sessionCookie[1].StartsWith(TimeOutEqual, StringComparison.OrdinalIgnoreCase))
                                    int.TryParse(sessionCookie[1].Substring(TimeOutEqual.Length).Trim(), out timeOut);

                                //  This automatically links the constructed object to its parent.
                                session = new RtspSession(_presentation, _setup.Connection, sessionCookie[0], timeOut, _setup.Url);
                            }
                        }
                    }

                    Session rtpSession = _setup.Process(response);
                    Tcs.SetResult(rtpSession);
                }
                catch (Exception ex)
                {
                    Interlocked.CompareExchange(ref _presentation._exception, ex, null);
                    Tcs.SetException(_presentation._exception);
                    _presentation._logger.ErrorFormat("[{0}] StreamSetup.ProcessSetupResponse() set exception", ex, _presentation._loggingId);
                }

                _presentation._logger.TraceFormat("[{0}] StreamSetup.ProcessSetupResponse() exiting", _presentation._loggingId);
            }
        }

        private Uri BuildSetupUrl(Media media)
        {
            ControlAttribute mediaControl = media.OfType<ControlAttribute>().FirstOrDefault();
            if (mediaControl != null && mediaControl.Url != null)
            {
                if (mediaControl.Url.IsAbsoluteUri)
                    return mediaControl.Url;

                UriBuilder builder = new UriBuilder(_baseUrl ?? _url);

                //  The Uri path separator is the first char in the path.
                builder.Path = builder.Path + builder.Path[0] + mediaControl.Url.OriginalString;
                return builder.Uri;
            }

            return _baseUrl == null ? _url : _baseUrl;
        }

        /// <summary>
        /// Start a request to start sending data via the mechanism specified in SETUP.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        /// <exception cref="InvalidOperationException">
        /// The list of sessions to play is empty.
        /// </exception>
        public Task PlayAsync()
        {
            _logger.TraceFormat("[{0}] PlayAsync() called", _loggingId);

            if (_sessions == null)
                throw new InvalidOperationException("No streams setup to play");

            bool created = false;

            //  Re-using SessionDescription as the lock object...
            lock (SessionDescription)
            {
                if (_playTCS == null)
                {
                    _playTCS = new TaskCompletionSource<string>();
                    created = true;
                }
            }

            if (created)
            {
                //  From now on any exception should be caught and set on the task returned
                //  to the caller...
                try
                {
                    //  If there is only a single RTSP session then send PLAY using
                    //  the presentation url.
                    if (_sessions._next == null)
                        _sessions._sessionUrl = _url;

                    RtspSession session = _sessions;
                    while (session != null)
                    {
                        Play play = new Play(session._sessionUrl, session.ProcessPlayResponse, _timeout, session._sessionCookie)
                                            { AuthenticationProvider = _authenticationProvider };
                        session._connection.Send(play);
                        session = session._next;
                    }
                }
                catch (Exception ex)
                {
                    Interlocked.CompareExchange(ref _exception, ex, null);
                    _playTCS.SetException(_exception);
                }
            }

            _logger.TraceFormat("[{0}] PlayAsync() returning", _loggingId);

            return _playTCS.Task;
        }

        /// <summary>
        /// Start a request to stop the stream delivery.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        /// <exception cref="InvalidOperationException">
        /// The list of sessions is empty or streaming was never started.
        /// </exception>
        public Task StopAsync()
        {
            _logger.TraceFormat("[{0}] StopAsync() called", _loggingId);

            if (_playTCS == null)
                throw new InvalidOperationException("Streaming not started");

            bool created = false;

            //  Re-using SessionDescription as the lock object...
            lock (SessionDescription)
            {
                if (_stopTCS == null)
                {
                    _stopTCS = new TaskCompletionSource<string>();
                    //  The TaskContinuationOptions.ExecuteSynchronously is important!
                    //  It eliminates a race condition in ClientConnection when setting/testing
                    //  _disposed.
                    _stopTCS.Task.ContinueWith(TearDownAsyncContinuation, TaskContinuationOptions.ExecuteSynchronously);
                    created = true;
                }
            }

            if (created)
            {
                //  If there is only a single RTSP session then send TEARDOWN using the
                //  presentation Url. Session Url was already set to the presentation url
                //  in PlayAsync().
                RtspSession session = _sessions;
                while (session != null)
                {
                    //  Dispose now but reset _keepAlive to null when processing the response.
                    session.Dispose();

                    try
                    {
                        TearDown teardown = new TearDown(session._sessionUrl, session.ProcessTeardownResponse, _timeout, session._sessionCookie)
                                                        { AuthenticationProvider = _authenticationProvider };
                        session._connection.Send(teardown);
                    }
                    catch (Exception ex)
                    {
                        //  If a keep-alive has triggered an exception use it instead of this
                        //  exception that has been thrown later...
                        Interlocked.CompareExchange(ref _exception, ex, null);
                    }

                    session = session._next;
                }

                //  It is possible the keep-alive processing has assigned an exception
                //  already. In this case it will be the keep-alive exception that is
                //  propagated to the stop task.
                if (_exception != null)
                    _stopTCS.SetException(_exception);
            }

            _logger.TraceFormat("[{0}] StopAsync() returning", _loggingId);

            return _stopTCS.Task;
        }

        /// <summary>
        /// Continuation run after the TEARDOWN has been executed.
        /// </summary>
        /// <remarks>
        /// This method may not throw an exception because it is a task that will not be
        /// watched.
        /// </remarks>
        /// <param name="tearDownTask"></param>
        private void TearDownAsyncContinuation(Task tearDownTask)
        {
            try
            {
                //  Each media stream that has been setup can now be closed.
                foreach (DictionaryEntry de in _streams)
                {
                    StreamSetup setup = de.Value as StreamSetup;
                    if (setup != null)
                    {
                        //  It is highly likely the same connection object in all streams but
                        //  calling Dispose() multiple times is permitted.
                        IDisposable disposable = setup._setup.Connection as IDisposable;
                        if (disposable != null)
                            disposable.Dispose();

                        setup._setup.CloseSessionConsumers();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// An RTSP session having its own session cookie on the associated connection.
        /// </summary>
        /// <remarks>
        /// A keep-alive timer is created for each session to support having different
        /// timeout values according to the Session header.
        /// </remarks>
        private sealed class RtspSession : IDisposable
        {
            public static RtspSession GetSession(RtspSession root, IClientConnection connection)
            {
                while (root != null)
                {
                    if (root._connection == connection)
                        return root;
                    root = root._next;
                }
                return null;
            }

            public readonly Presentation _parent;
            public readonly IClientConnection _connection;
            public readonly string _sessionCookie;
            public readonly int _timeOut;
            public Uri _sessionUrl;
            public Timer _keepAlive;
            public RtspSession _next;

            public RtspSession(Presentation parent, IClientConnection connection, string sessionCookie, int timeOut, Uri sessionUrl)
            {
                _parent = parent;
                _connection = connection;
                _sessionCookie = sessionCookie;
                _timeOut = timeOut;
                _sessionUrl = sessionUrl;

                if (_parent._sessions == null)
                    _parent._sessions = this;
                else
                {
                    RtspSession root = _parent._sessions;
                    while (root._next != null)
                        root = root._next;
                    root._next = this;
                }
            }

            /// <summary>
            /// Delegate to run when receiving session PLAY responses.
            /// </summary>
            /// <remarks>
            /// This method may not throw an exception because it is called from "lower level"
            /// networking code. Any exception is to be set on the TaskCompletionSource used
            /// to give a Task to the external code.
            /// </remarks>
            /// <param name="request"></param>
            /// <param name="response"></param>
            /// <param name="exception"></param>
            public void ProcessPlayResponse(Request request, Response response, Exception exception)
            {
                _parent._logger.TraceFormat("[{0}] RtspSession.ProcessPlayResponse() entered", _parent._loggingId);

                if (exception != null)
                {
                    try
                    {
                        Interlocked.CompareExchange(ref _parent._exception, exception, null);
                        _parent._playTCS.SetException(_parent._exception);
                    }
                    //  Swallow a possible race condition exception.
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    //  Start this sessions keep alive sender.
                    using (AsyncFlowControl afc = ExecutionContext.SuppressFlow())
                    {
                        _keepAlive = new Timer(KeepAliveSender, null, _timeOut * 1000, _timeOut * 1000);
                    }

                    //  Check if all RTSP sessions have returned successfully because then
                    //  the Play task is to be started.
                    //  There should not be any need of a lock when looping through the
                    //  sessions because if it occured it will be handled by the below
                    //  catch.
                    bool okToStartPlayTask = true;

                    RtspSession session = _parent._sessions;
                    while (session != null)
                    {
                        if (session._keepAlive == null)
                        {
                            okToStartPlayTask = false;
                            break;
                        }
                        session = session._next;
                    }

                    if (okToStartPlayTask)
                    {
                        try
                        {
                            _parent._playTCS.SetResult(null);
                        }
                        //  Swallow a possible race condition exception.
                        catch (Exception)
                        {
                        }
                    }
                }

                _parent._logger.TraceFormat("[{0}] RtspSession.ProcessPlayResponse() exiting", _parent._loggingId);
            }

            /// <summary>
            /// Timer callback that sends the keep-alive for this session.
            /// </summary>
            /// <param name="state"></param>
            private void KeepAliveSender(object state)
            {
                _parent._logger.TraceFormat("[{0}] RtspSession.KeepAliveSender() entered", _parent._loggingId);

                try
                {
                    GetParameter gp = new GetParameter(_sessionUrl, KeepAliveResponse, _parent._timeout)
                                                      { AuthenticationProvider = _parent._authenticationProvider };
                    _connection.Send(gp);
                }
                catch (Exception ex)
                {
                    //  The communication is broken down if exception in send.
                    Interlocked.CompareExchange(ref _parent._exception, ex, null);
                    _parent.StopAsync();
                }

                _parent._logger.TraceFormat("[{0}] RtspSession.KeepAliveSender() exiting", _parent._loggingId);
            }

            private void KeepAliveResponse(Request request, Response response, Exception exception)
            {
                if (exception != null)
                {
                    //  If it is an RtspException assume it is an invalid command error. That is
                    //  actually OK and should not stop the streaming.
                    //  (Assuming RTSP command and even an error respons processed by the server
                    //  should be considered as a keep-alive.)
                    if (!(exception is RtspException))
                    {
                        Interlocked.CompareExchange(ref _parent._exception, exception, null);
                        _parent.StopAsync();
                    }
                }
            }

            /// <summary>
            /// Delegate to run when receiving the TEARDOWN response.
            /// </summary>
            /// <remarks>
            /// This method may not throw an exception because it is called from "lower level"
            /// networking code. Any exception is to be set on the TaskCompletionSource used
            /// to give a Task to the external code.
            /// </remarks>
            /// <param name="request"></param>
            /// <param name="response"></param>
            /// <param name="exception"></param>
            public void ProcessTeardownResponse(Request request, Response response, Exception exception)
            {
                _parent._logger.TraceFormat("[{0}] RtspSession.ProcessTeardownResponse() entered", _parent._loggingId);

                if (exception != null)
                {
                    try
                    {
                        Interlocked.CompareExchange(ref _parent._exception, exception, null);
                        _parent._stopTCS.SetException(_parent._exception);
                    }
                    //  Swallow a possible race condition exception.
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    //  Reset the keep alive timer to indicate succesfull response.
                    _keepAlive = null;

                    //  Check if all RTSP sessions have returned successfully because then
                    //  the Stop task is to be started.
                    //  There should not be any need of a lock when looping through the
                    //  sessions because if it occured it will be handled by the below
                    //  catch.
                    bool okToStartStopTask = true;

                    RtspSession session = _parent._sessions;
                    while (session != null)
                    {
                        if (session._keepAlive != null)
                        {
                            okToStartStopTask = false;
                            break;
                        }
                        session = session._next;
                    }

                    if (okToStartStopTask)
                    {
                        try
                        {
                            _parent._stopTCS.SetResult(null);
                        }
                        //  Swallow a possible race condition exception.
                        catch (Exception)
                        {
                        }
                    }
                }

                _parent._logger.TraceFormat("[{0}] RtspSession.ProcessTeardownResponse() exiting", _parent._loggingId);
            }

            public void Dispose()
            {
                _keepAlive.Dispose();
            }
        }
    }
}
