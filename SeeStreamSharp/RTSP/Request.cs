﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Text;

namespace SeeStreamSharp.RTSP
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    internal sealed class RequestNameAttribute : System.Attribute
    {
        private readonly string _cmdName;

        // This is a positional argument
        public RequestNameAttribute(string commandName)
        {
            _cmdName = commandName;
        }

        public string CommandName
        {
            get { return _cmdName; }
        }
    }

    internal abstract class Request : Message
    {
        private static readonly char[] SPACE_DELIMITER = new char[] { ' ' };
        private static readonly Dictionary<string, Type> s_commands = new Dictionary<string, Type>();

        static Request()
        {
            Assembly a = Assembly.GetExecutingAssembly();
            foreach (Type t in a.GetTypes())
            {
                if (t.IsSubclassOf(typeof(Request)))
                {
                    System.Attribute[] attribs = System.Attribute.GetCustomAttributes(t);
                    foreach (System.Attribute attr in attribs)
                    {
                        RequestNameAttribute cmdName = attr as RequestNameAttribute;
                        if (cmdName != null)
                        {
                            Requests result;
                            if (Enum.TryParse<Requests>(cmdName.CommandName, out result))
                            {
                                s_commands.Add(cmdName.CommandName, t);
                            }
                            break;
                        }
                    }
                }
            }
        }

        public static Request ParseRequestLine(string line)
        {
            string[] requestParts = line.Split(SPACE_DELIMITER);
            if (requestParts.Length == 3)
            {
                foreach (KeyValuePair<string, Type> cmd in s_commands)
                {
                    if (cmd.Key.Equals(requestParts[0], StringComparison.Ordinal))
                    {
                        Uri uri = new Uri(requestParts[1], UriKind.Absolute);
                        Request request = Activator.CreateInstance(cmd.Value, uri) as Request;
                        return request;
                    }
                }
            }

            return null;
        }

        private readonly Uri _uri;
        private readonly Action<Request, Response, Exception> _responseAction;
        private readonly int _timeOut;

        public Uri Url { get { return _uri; } }

        public int TimeOut { get { return _timeOut; } }

        public AuthenticationProvider AuthenticationProvider { get; set; }

        protected Request(Requests cmd, Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(cmd)
        {
            _uri = uri;
            _responseAction = responseAction;
            _timeOut = timeout;
        }

        internal void SetCSeq(int number)
        {
            CSeq = number;
        }

        protected sealed override string FirstLineToString()
        {
            //  DESCRIBE rtsp://live.example.com/concert/audio RTSP/1.0
            return string.Format("{0} {1} RTSP/1.0", Request.ToString(), _uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));
        }

        internal void Process(Response response, Exception ex, IClientConnection clientconnection)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized && AuthenticationProvider != null)
            {
                if (AuthenticationProvider.Unauthorized(clientconnection, response, this))
                {
                    clientconnection.Send(this);
                    return;
                }
            }

            if (_responseAction != null)
                _responseAction(this, response, ex);
        }
    }

    [RequestName("OPTIONS")]
    internal class Options : Request
    {
        public Options(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.OPTIONS, uri, responseAction, timeout)
        {
        }
    }

    [RequestName("DESCRIBE")]
    internal class Describe : Request
    {
        private const string mimeType = "application/sdp";

        public Describe(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.DESCRIBE, uri, responseAction, timeout)
        {
            //  This is the only description format this client understands.
            Headers.Add(HeaderCollection.Accept, mimeType);
        }

        internal SessionDescription Process(Response response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new RtspException(string.Format("DESCRIBE request got an '{0}' status code response, reason phrase = '{1}'",
                    (int)response.StatusCode, response.ReasonPhrase));
            }

            Encoding encoder = Encoding.UTF8;

            ContentType contentType = response.Headers.ContentTypeValue;
            if (contentType != null)
            {
                if (!contentType.MediaType.Equals(mimeType, StringComparison.Ordinal))
                {
                    throw new RtspException(string.Format("DESCRIBE response contains unexpected content type '{0}'",
                        contentType.MediaType));
                }

                if (!string.IsNullOrEmpty(contentType.CharSet))
                    encoder = Encoding.GetEncoding(contentType.CharSet);
            }

            if (response.Entity == null || response.Entity.Length == 0)
                throw new RtspException("DESCRIBE response does not contain any data");

            return SessionDescription.Parse(encoder.GetString(response.Entity));
        }
    }

    [RequestName("SETUP")]
    internal class Setup : Request
    {
        //  Session
        private const string timeoutPrefix = "timeout=";
        //  Transport
        private const string interleavedPrefix = "interleaved=";
        private const string clientPortPrefix = "client_port=";
        private const string serverPortPrefix = "server_port=";
        private const string unicastParam = "unicast";
        private const string multicastParam = "multicast";

        private string _transportProtocol;

        private IClientConnection _connection;
        private int _reservedChannel;
        private bool _interleaved;

        private int _startPort = -1;

        public IClientConnection Connection { get { return _connection; } }

        public Setup(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.SETUP, uri, responseAction, timeout)
        {
        }

        public void SetInterleavedTcpTransport(string transportProtocol, IClientConnection connection)
        {
            _transportProtocol = transportProtocol;
            _connection = connection;
            _reservedChannel = connection.ReserveEvenChannelNumber();
            _interleaved = true;

            Headers.Set(HeaderCollection.Transport, _transportProtocol);
            //  One RTSP camera responds with "400, Bad request" if "unicast" is omitted.
            Headers.Add(HeaderCollection.Transport, unicastParam);
            string clientPort = string.Format("{2}{0}-{1}", _reservedChannel, _reservedChannel + 1, interleavedPrefix);
            Headers.Add(HeaderCollection.Transport, clientPort);
        }

        public void SetUnicastUdpTransport(string transportProtocol, int startPort)
        {
            _transportProtocol = transportProtocol;
            _startPort = startPort;
            _interleaved = false;

            Headers.Set(HeaderCollection.Transport, _transportProtocol);
            Headers.Add(HeaderCollection.Transport, unicastParam);
            string clientPort = string.Format("{2}{0}-{1}", startPort, startPort + 1, clientPortPrefix);
            Headers.Add(HeaderCollection.Transport, clientPort);
        }

        public Session Process(Response response)
        {
            if (response.StatusCode == Response.UnsupportedTransport)
            {
                if (_connection != null)
                {
                    _connection.UnreserveChannel(_reservedChannel);
                    _connection = null;
                }
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new RtspException(string.Format("SETUP request got an '{0}' status code response, reason phrase = '{1}'",
                    (int)response.StatusCode, response.ReasonPhrase));
            }

            string[] values = response.Headers.GetValues(HeaderCollection.Session);
            if (values != null)
            {
                if (values.Length > 1)
                {
                    int timeout;
                    int.TryParse(values[1].Substring(timeoutPrefix.Length), out timeout);
                }
            }

            values = response.Headers.GetValues(HeaderCollection.Transport);
            if (values == null)
                throw new RtspException("SETUP response does not contain a Transport header line");

            int startInterleavedChannel = 0;
            int startClientPort = 0;
            int startServerPort = 0;

            for (int index = 0; index < values.Length; index++)
            {
                //  The transport protocol is always the start of all proposals and responses.
                //  This client only sends one proposal and expects a single response.
                if (index == 0)
                {
                    if (!values[index].Equals(_transportProtocol, StringComparison.OrdinalIgnoreCase))
                        throw new RtspException("Transport header response does not match request");
                }

                if (_interleaved && values[index].StartsWith(interleavedPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    int firstValueEnd = values[index].IndexOf('-', interleavedPrefix.Length);
                    if (firstValueEnd != -1)
                    {
                        //  Resets interleaved if the channel number cant be parsed.
                        _interleaved = int.TryParse(values[index].Substring(interleavedPrefix.Length, firstValueEnd - interleavedPrefix.Length),
                                                   out startInterleavedChannel);
                    }
                }

                if (!_interleaved && values[index].StartsWith(clientPortPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    int firstValueEnd = values[index].IndexOf('-', clientPortPrefix.Length);
                    if (firstValueEnd != -1)
                    {
                        int.TryParse(values[index].Substring(clientPortPrefix.Length, firstValueEnd - clientPortPrefix.Length),
                                     out startClientPort);
                    }
                }

                if (!_interleaved && values[index].StartsWith(serverPortPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    int firstValueEnd = values[index].IndexOf('-', serverPortPrefix.Length);
                    if (firstValueEnd != -1)
                    {
                        int.TryParse(values[index].Substring(serverPortPrefix.Length, firstValueEnd - serverPortPrefix.Length),
                                     out startServerPort);
                    }
                }
            }

            Session session = null;
            if (_interleaved)
            {
                _connection.UnreserveChannel(_reservedChannel);
                if (startInterleavedChannel != _reservedChannel)
                    _reservedChannel = startInterleavedChannel;

                session = new Session();
                PacketTransceiver rtp = new PacketTransceiver(session.RtpObservers);
                RtcpPacketTransceiver rtcp = new RtcpPacketTransceiver(session.RtcpObservers);

                _connection.AddChannel(_reservedChannel, rtp);
                _connection.AddChannel(_reservedChannel + 1, rtcp);
            }

            return session;
        }

        public void CloseSessionConsumers()
        {
            if (_interleaved)
            {
                _connection.RemoveChannel(_reservedChannel);
                _connection.RemoveChannel(_reservedChannel + 1);
            }
        }
    }

    [RequestName("PLAY")]
    internal class Play : Request
    {
        //  Range
        //private const string playTimePrefix = "npt=";

        public Play(Uri uri, Action<Request, Response, Exception> responseAction, int timeout, string sessionCokie)
            : base(Requests.PLAY, uri, responseAction, timeout)
        {
            //string playTime = string.Format("{2}{0}-{1}", 0, 10, playTimePrefix);
            //Headers.Set(HeaderCollection.Range, playTime);
            Headers.Set(HeaderCollection.Session, sessionCokie);
        }
    }

    [RequestName("PAUSE")]
    internal class Pause : Request
    {
        public Pause(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.PAUSE, uri, responseAction, timeout)
        {

        }
    }

    [RequestName("TEARDOWN")]
    internal class TearDown : Request
    {
        public TearDown(Uri uri, Action<Request, Response, Exception> responseAction, int timeout, string sessionCokie)
            : base(Requests.TEARDOWN, uri, responseAction, timeout)
        {
            Headers.Set(HeaderCollection.Session, sessionCokie);
        }
    }

    [RequestName("GET_PARAMETER")]
    internal class GetParameter : Request
    {
        public GetParameter(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.GET_PARAMETER, uri, responseAction, timeout)
        {

        }
    }

    [RequestName("SET_PARAMETER")]
    internal class SetParameter : Request
    {
        public SetParameter(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.SET_PARAMETER, uri, responseAction, timeout)
        {

        }
    }

    [RequestName("ANNOUNCE")]
    internal class Announce : Request
    {
        public Announce(Uri uri, Action<Request, Response, Exception> responseAction, int timeout)
            : base(Requests.ANNOUNCE, uri, responseAction, timeout)
        {

        }
    }
}
