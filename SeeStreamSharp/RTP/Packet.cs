﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Encapsulates the RTP header and the associated payload byte array.
    /// </summary>
    /// <remarks>
    /// PLEASE NOTE!
    /// <para>
    /// Be very carefull on how to use the byte arrays when received using the
    /// <see cref="IObservable{T}"/> interface on <see cref="Session"/>!
    /// </para>
    /// <para>
    /// If byte arrays needs to be available after returning from the call to
    /// <see cref="IObserver{T}.OnNext(T)"/> they have to be copied. It is not even possbile
    /// to hold on to the byte array reference in any of the <see cref="ArraySegment{T}"/>.
    /// After returning from the notification these byte arrays will be overwritten.
    /// </para>
    /// <para>
    /// This is intentionally a struct. It is only intended to be passed as a single argument
    /// to <see cref="IObserver{T}.OnNext(T)"/> and in so avoid an allocation on the heap.
    /// The member fields are not declared readonly because the struct will be copied on
    /// method calls.
    /// </para>
    /// </remarks>
    public struct Packet
    {
        public RtpFixedHeader Header;
        public ArraySegment<byte> Csrc;
        public ArraySegment<byte> ExtendedHeader;
        public ArraySegment<byte> Payload;

        public Packet(RtpFixedHeader header, ArraySegment<byte> csrc,
            ArraySegment<byte> extendedHeader, ArraySegment<byte> payload)
        {
            Header = header;
            Csrc = csrc;
            ExtendedHeader = extendedHeader;
            Payload = payload;
        }
    }
}
