﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Threading;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Enables multiple subscribers on a specific packet type in a RTP session.
    /// </summary>
    /// <typeparam name="T"><see cref="Packet"/> or <see cref="RtcpFixedHeader"/></typeparam>
    internal class SessionSubject<T> : IObserver<T>
    {
        internal delegate void Processor(ref T instance);

        private readonly Processor _processor;
        private volatile object _observers;
        private Exception _error;

        internal SessionSubject(Processor processor)
        {
            _processor = processor;
        }

        internal IDisposable Subscribe(IObserver<T> observer)
        {
            object current;
            object replacement = null;

            do
            {
                current = _observers;

                if (current == NopDisposable.Instance)
                {
                    if (_error != null)
                        observer.OnError(_error);
                    else
                        observer.OnCompleted();

                    return NopDisposable.Instance;
                }

                if (current == null)
                    replacement = observer;
                else
                {
                    IObserver<T>[] currentArray = current as IObserver<T>[];
                    if (currentArray != null)
                    {
                        IObserver<T>[] newArray = new IObserver<T>[currentArray.Length + 1];

                        Array.Copy(currentArray, newArray, currentArray.Length);
                        newArray[currentArray.Length] = observer;
                        replacement = newArray;
                    }
                    else
                    {
                        IObserver<T> firstObserver = current as IObserver<T>;
                        replacement = new IObserver<T>[] { firstObserver, observer };
                    }
                }
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            return new Subscription(this, observer);
        }

        private void Unsubscribe(IObserver<T> observer)
        {
            object current;
            object replacement = null;

            do
            {
                current = _observers;

                if (current == NopDisposable.Instance)
                    return;

                IObserver<T>[] currentArray = current as IObserver<T>[];
                if (currentArray != null)
                {
                    int i = currentArray.Length - 1;
                    do
                    {
                        if (currentArray[i] == observer)
                            break;
                        i--;
                    } while (i >= 0);

                    //  Argument is not in the list, bail out.
                    if (i < 0)
                        return;

                    int length = currentArray.Length;
                    if (length == 1)
                        replacement = null;
                    else
                    {
                        IObserver<T>[] newArray = new IObserver<T>[currentArray.Length - 1];
                        Array.Copy(currentArray, 0, newArray, 0, i);
                        Array.Copy(currentArray, i + 1, newArray, i, length - i - 1);
                        replacement = newArray;
                    }
                }
                else
                {
                    if (current != observer)
                        return;
                    replacement = null;
                }
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);
        }

        void IObserver<T>.OnNext(T value)
        {
            if (_processor != null)
                _processor(ref value);

            IObserver<T> observer = _observers as IObserver<T>;
            if (observer != null)
            {
                observer.OnNext(value);
                return;
            }
            IObserver<T>[] observers = _observers as IObserver<T>[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].OnNext(value);
                }
            }
        }

        void IObserver<T>.OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            //  If not yet set then assign the error.
            if (Interlocked.CompareExchange(ref _error, error, null) != null)
                return;

            object current;
            object replacement = NopDisposable.Instance;

            do
            {
                current = _observers;
                if (current == NopDisposable.Instance)
                    break;
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            IObserver<T> observer = current as IObserver<T>;
            if (observer != null)
            {
                observer.OnError(error);
                return;
            }
            IObserver<T>[] observers = current as IObserver<T>[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].OnError(error);
                }
            }
        }

        void IObserver<T>.OnCompleted()
        {
            object current;
            object replacement = NopDisposable.Instance;

            do
            {
                current = _observers;
                if (current == NopDisposable.Instance)
                    break;
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            IObserver<T> observer = current as IObserver<T>;
            if (observer != null)
            {
                observer.OnCompleted();
                return;
            }
            IObserver<T>[] observers = current as IObserver<T>[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].OnCompleted();
                }
            }
        }

        private class Subscription : IDisposable
        {
            private SessionSubject<T> _subject;
            private IObserver<T> _observer;

            public Subscription(SessionSubject<T> subject, IObserver<T> observer)
            {
                _subject = subject;
                _observer = observer;
            }

            public void Dispose()
            {
                //  Make sure that Unsubscribe() is only called one time.
                IObserver<T> observer = Interlocked.Exchange(ref _observer, null);
                if (observer != null)
                    return;

                _subject.Unsubscribe(observer);
                _subject = null;
            }
        }

        private class NopDisposable : IDisposable
        {
            public static IDisposable Instance = new NopDisposable();

            private NopDisposable()
            {
            }

            public void Dispose()
            {
                //  Does nothing.
            }
        }
    }

}
