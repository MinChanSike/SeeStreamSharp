﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SeeStreamSharp.SDP
{
    /// <summary>
    /// Provides a Session Description type=value abstraction.
    /// </summary>
    public abstract class TypeValue
    {
        private static readonly string[] SupportedNetworkTypes = new string[] { "IN" };
        private static readonly string[] SupportedAddressTypes = new string[] { "IP4", "IP6" };
        private static readonly char[] spaceDelimiter = new char[] { ' ' };
        private static readonly char[] slashDelimiter = new char[] { '/' };

        /// <summary>
        /// The <see cref="SessionDescription"/> this is owned by.
        /// </summary>
        public readonly SessionDescription Session;

        /// <summary>
        /// The type of this object.
        /// </summary>
        public readonly string Type;

        /// <summary>
        /// The parent <see cref="Media"/> object or null if session level.
        /// </summary>
        public readonly Media MediaParent;

        /// <summary>
        /// Initialize a base object.
        /// </summary>
        /// <param name="session">The owning <see cref="SessionDescription"/>.</param>
        /// <param name="type">The <see cref="string"/> type.</param>
        /// <param name="mediaParent">A possible <see cref="Media"/> owner.</param>
        protected TypeValue(SessionDescription session, string type, Media mediaParent)
        {
            Session = session;
            Type = type;
            MediaParent = mediaParent;
        }

        /// <summary>
        /// Validate if network type is implemented.
        /// </summary>
        /// <param name="networkType">Network type string.</param>
        /// <returns>True if implemented.</returns>
        protected static bool IsSupportedNetoworkType(string networkType)
        {
            return SupportedNetworkTypes.Contains(networkType);
        }

        /// <summary>
        /// Validate if address type is implemented.
        /// </summary>
        /// <param name="addressType">Address type string.</param>
        /// <returns>True if implemented.</returns>
        protected static bool IsSupportedAddressType(string addressType)
        {
            return SupportedAddressTypes.Contains(addressType);
        }

        /// <summary>
        /// SDP field split using space.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected static string[] SpaceSplit(string input)
        {
            return input.Split(spaceDelimiter);
        }

        /// <summary>
        /// SDP field split using slash.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected static string[] SlashSplit(string input)
        {
            return input.Split(slashDelimiter);
        }
    }

    /// <summary>
    /// The session level protocol version.
    /// </summary>
    public class Version : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "v";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Version type parser (v=) called with invalid line = " + line);

            int result;
            if (int.TryParse(line.Substring(2), out result))
                return new Version(session, result);

            return null;
        }

        public readonly int VersionValue;

        /// <summary>
        /// Create a version <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="version"></param>
        public Version(SessionDescription session, int version)
            : base(session, TypeName, null)
        {
            VersionValue = version;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, VersionValue);
        }
    }

    /// <summary>
    /// The session level owner/creator and session identifier.
    /// </summary>
    public class Origin : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "o";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Origin type parser (o=) called with invalid line = " + line);

            string[] fields = SpaceSplit(line.Substring(2));

            if (fields.Length != 6)
                throw new InvalidSessionDescriptionException("Origin type called with invalid line = " + line);

            long sessionId = 0;
            if (!long.TryParse(fields[1], out sessionId))
                throw new InvalidSessionDescriptionException("Invalid Origin type Session ID, line = " + line);

            long version = 0;
            if (!long.TryParse(fields[2], out version))
                throw new InvalidSessionDescriptionException("Invalid Origin type Version, line = " + line);

            if (!IsSupportedNetoworkType(fields[3]))
                throw new InvalidSessionDescriptionException("Invalid Origin network type = " + fields[3]);

            if (!IsSupportedAddressType(fields[4]))
                throw new InvalidSessionDescriptionException("Invalid Origin addres type = " + fields[4]);

            IPAddress ipAddress = null;
            string hostName = null;
            try
            {
                ipAddress = IPAddress.Parse(fields[5]);
            }
            catch (FormatException)
            {
                //  Assume for now that this contain a host name.
                hostName = fields[5];
            }

            return new Origin(session, mediaParent, fields[0], sessionId, version, ipAddress, hostName);
        }

        public readonly string UserName;
        public readonly long SessionId;
        public readonly long Version;
        public readonly IPAddress IpAddress;
        public readonly string HostName;

        /// <summary>
        /// Create an origin <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="userName"></param>
        /// <param name="sessionId"></param>
        /// <param name="version"></param>
        /// <param name="ipAddress"></param>
        /// <param name="hostName"></param>
        public Origin(SessionDescription session, Media mediaParent, string userName,
                         long sessionId, long version, IPAddress ipAddress, string hostName)
            : base(session, TypeName, mediaParent)
        {
            UserName = userName;
            SessionId = sessionId;
            Version = version;
            IpAddress = ipAddress;
            HostName = hostName;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            string address = string.Empty;
            string addressType = "IP4";

            if (IpAddress != null)
            {
                address = IpAddress.ToString();
                if (IpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    addressType = "IP6";
            }
            if (HostName != null)
                address = HostName;

            return string.Format("{0}={1} {2} {3} IN {4} {5}", TypeName, UserName,
                                 SessionId.ToString(), Version.ToString(),
                                 addressType, address);
        }
    }

    /// <summary>
    /// The session level session name.
    /// </summary>
    public class SessionName : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "s";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("SessionName type parser (s=) called with invalid line = " + line);

            return new SessionName(session, mediaParent, line.Substring(2));
        }

        public readonly string Name;

        /// <summary>
        /// Create a session name <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="name"></param>
        public SessionName(SessionDescription session, Media mediaParent, string name)
            : base(session, TypeName, mediaParent)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, Name);
        }
    }

    /// <summary>
    /// The session level session information.
    /// </summary>
    public class SessionAndMediaInformation : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "i";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("SessionAndMediaInformation type parser (i=) called with invalid line = " + line);

            return new SessionAndMediaInformation(session, mediaParent, line.Substring(2));
        }

        public readonly string Value;

        /// <summary>
        /// Create a session and media information <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="sessionAndMediaInformation"></param>
        public SessionAndMediaInformation(SessionDescription session, Media mediaParent, string sessionAndMediaInformation)
            : base(session, TypeName, mediaParent)
        {
            Value = sessionAndMediaInformation;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, Value);
        }
    }

    /// <summary>
    /// The session level URI of description.
    /// </summary>
    public class Url : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "u";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Uri type parser (u=) called with invalid line = " + line);

            return new Url(session, mediaParent, new System.Uri(line.Substring(2)));
        }

        public readonly System.Uri Ur1;

        /// <summary>
        /// Create an url <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="uri"></param>
        public Url(SessionDescription session, Media mediaParent, System.Uri uri)
            : base(session, TypeName, mediaParent)
        {
            Ur1 = uri;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, Ur1.ToString());
        }
    }

    /// <summary>
    /// The session level email address.
    /// </summary>
    public class Email : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "e";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Email type parser (e=) called with invalid line = " + line);

            return new Email(session, mediaParent, line.Substring(2));
        }

        public readonly string EmailValue;

        /// <summary>
        /// Create an e-mail <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="email"></param>
        public Email(SessionDescription session, Media mediaParent, string email)
            : base(session, TypeName, mediaParent)
        {
            EmailValue = email;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, EmailValue);
        }
    }

    /// <summary>
    /// The session level phone number.
    /// </summary>
    public class Phone : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "p";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Phone type parser (p=) called with invalid line = " + line);

            return new Phone(session, mediaParent, line.Substring(2));
        }

        public readonly string PhoneValue;

        /// <summary>
        /// Create a phone <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="phone"></param>
        public Phone(SessionDescription session, Media mediaParent, string phone)
            : base(session, TypeName, mediaParent)
        {
            PhoneValue = phone;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, PhoneValue);
        }
    }

    /// <summary>
    /// The session or media level connection information.
    /// </summary>
    public class Connection : TypeValue
    {
        private const int DEFAULT_TTL = 255;
        private const int DEFAULT_NUM_ADDRESSES = 1;
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "c";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Connection type parser (c=) called with invalid line = " + line);

            string[] fields = SpaceSplit(line.Substring(2));
            if (fields.Length != 3)
                throw new InvalidSessionDescriptionException("Invalid number of sub fields in Connection type (c=) line = " + line);

            if (!IsSupportedNetoworkType(fields[0]))
                throw new InvalidSessionDescriptionException("Invalid Connection network type = " + fields[0]);

            IPAddress ipAddress = null;
            bool supportedAddressType = IsSupportedAddressType(fields[1]);
            if (!supportedAddressType)
                throw new InvalidSessionDescriptionException("Invalid Connection address type = " + fields[1]);

            string hostName = null;
            string[] addressParts = SlashSplit(fields[2]);
            int ttl = DEFAULT_TTL;
            int numberOfAddresses = DEFAULT_NUM_ADDRESSES;
            try
            {
                ipAddress = IPAddress.Parse(addressParts[0]);
            }
            catch (FormatException)
            {
                //  Assume for now that this contain a host name.
                hostName = addressParts[0];
            }

            if (addressParts.Length > 1)
                int.TryParse(addressParts[1], out ttl);

            if (addressParts.Length > 2)
                int.TryParse(addressParts[2], out numberOfAddresses);

            return new Connection(session, mediaParent, ipAddress, hostName, ttl, numberOfAddresses);
        }

        public readonly IPAddress IpAddress;
        public readonly string HostName;
        public readonly int Ttl;
        public readonly int NumberOfAddresses;

        /// <summary>
        /// Create a connnection <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="ipAddress"></param>
        /// <param name="hostName"></param>
        /// <param name="ttl"></param>
        /// <param name="numberOfAddresses"></param>
        public Connection(SessionDescription session, Media mediaParent,
                             IPAddress ipAddress, string hostName,
                             int ttl, int numberOfAddresses)
            : base(session, TypeName, mediaParent)
        {
            IpAddress = ipAddress;
            HostName = hostName;
            Ttl = ttl;
            NumberOfAddresses = numberOfAddresses;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            string address = string.Empty,
                   addressType = "IP4",
                   noa = string.Empty;

            if (IpAddress != null)
            {
                address = IpAddress.ToString();
                if (IpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                {
                    addressType = "IP6";
                }
            }
            if (HostName != null)
                address = HostName;
            if (NumberOfAddresses != DEFAULT_NUM_ADDRESSES)
                noa = string.Format("/{0}", NumberOfAddresses);
            if (Ttl != DEFAULT_TTL || noa != string.Empty)
                noa = string.Format("/{0}{1}", Ttl, noa);

            return string.Format("{0}=IN {1} {2}{3}", TypeName, addressType, address, noa);
        }
    }

    /// <summary>
    /// The session or media level bandwidth information.
    /// </summary>
    public class Bandwidth : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "b";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Bandwidth type parser (b=) called with invalid line = " + line);

            int offset = line.IndexOf(':');
            if (offset == -1)
                throw new InvalidSessionDescriptionException("Bandwidth type parser (b=) called with invalid line = " + line);

            int bandwith = 0;
            if (!int.TryParse(line.Substring(offset + 1), out bandwith))
                throw new InvalidSessionDescriptionException("Bandwidth type parser (b=) called with invalid bandwith value, line = " + line);

            return new Bandwidth(session, mediaParent, line.Substring(2, offset - 2), bandwith);
        }

        public readonly string Modifier;
        public readonly int BandwithValue;

        /// <summary>
        /// Create a bandwith <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="modifier"></param>
        /// <param name="bandwith"></param>
        public Bandwidth(SessionDescription session, Media mediaParent, string modifier, int bandwith)
            : base(session, TypeName, mediaParent)
        {
            Modifier = modifier;
            BandwithValue = bandwith;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}:{2}", TypeName, Modifier, BandwithValue.ToString());
        }
    }

    /// <summary>
    /// The session or media level time the session is active
    /// </summary>
    public class Time : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "t";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Time type parser (t=) called with invalid line = " + line);

            string[] fields = SpaceSplit(line.Substring(2));

            uint start = 0,
                 stop = 0;
            if (!uint.TryParse(fields[0], out start))
                throw new InvalidSessionDescriptionException("Time type invalid start time value, line = " + line);
            if (!uint.TryParse(fields[1], out stop))
                throw new InvalidSessionDescriptionException("Time type invalid stop time value, line = " + line);

            return new Time(session, mediaParent, (int)start, (int)stop);
        }

        public readonly int Start,
                            Stop;

        /// <summary>
        /// Create a time <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaParent"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        public Time(SessionDescription session, Media mediaParent, int start, int stop)
            : base(session, TypeName, mediaParent)
        {
            Start = start;
            Stop = stop;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1} {2}", TypeName, ((uint)Start).ToString(), ((uint)Stop).ToString());
        }
    }

    /// <summary>
    /// The session level media name and transport address.
    /// </summary>
    public class Media : TypeValue, IEnumerable<TypeValue>
    {
        private const int DEFAULT_NUMBER_OF_PORTS = 1;
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "m";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Media type parser (m=) called with invalid line = " + line);

            string[] fields = SpaceSplit(line.Substring(2));

            SessionDescription.MediaType mt = session.ParseMediaType(fields[0]);
            int port = 0,
                nop = DEFAULT_NUMBER_OF_PORTS;

            string[] portSubfields = SlashSplit(fields[1]);
            if (!int.TryParse(portSubfields[0], out port))
                throw new InvalidSessionDescriptionException("Media type invalid port subfield, line = " + line);
            if (portSubfields.Length > 1)
            {
                if (!int.TryParse(portSubfields[1], out nop))
                    throw new InvalidSessionDescriptionException("Media type invalid number of ports subfield, line = " + line);
            }

            int[] formatArray = null;
            string[] formatStringArray = null;

            if (mt == SessionDescription.MediaType.audio || mt == SessionDescription.MediaType.video)
            {
                formatArray = new int[fields.Length - 3];
                for (int i = 3; i < fields.Length; i++)
                {
                    int fmt;

                    if (!int.TryParse(fields[i], out fmt))
                        throw new InvalidSessionDescriptionException("Media type invalid format list subfield, line = " + line);

                    formatArray[i - 3] = fmt;
                }
            }
            else
            {
                formatStringArray = new string[fields.Length - 3];
                Array.Copy(fields, 3, formatStringArray, 0, fields.Length - 3);
            }

            return new Media(session, mt, port, nop, fields[2], formatArray, formatStringArray);
        }

        private readonly List<TypeValue> _mediaDescription = new List<TypeValue>();
        public readonly SessionDescription.MediaType MediaType;
        public readonly int Port,
                            NumberOfPorts;
        public readonly string Transport;
        public readonly int[] FormatArray;
        public readonly string[] FormatStringArray;

        /// <summary>
        /// Create a media <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="mediaType"></param>
        /// <param name="port"></param>
        /// <param name="numberOfPorts"></param>
        /// <param name="transport"></param>
        /// <param name="formatArray"></param>
        /// <param name="formatStringArray"></param>
        public Media(SessionDescription session, SessionDescription.MediaType mediaType,
                        int port, int numberOfPorts, string transport,
                        int[] formatArray, string[] formatStringArray)
            : base(session, TypeName, null)
        {
            MediaType = mediaType;
            Port = port;
            NumberOfPorts = numberOfPorts;
            Transport = transport;
            FormatArray = formatArray;
            FormatStringArray = formatStringArray;
        }

        public void AddTypeValue(TypeValue typeValue)
        {
            _mediaDescription.Add(typeValue);
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            string nop = NumberOfPorts != DEFAULT_NUMBER_OF_PORTS ? string.Format("/{0}", NumberOfPorts.ToString()) : string.Empty;
            string port = string.Format("{0}{1}", Port.ToString(), nop);

            string fmtList = string.Empty;
            if (FormatArray != null)
                fmtList = string.Join<int>(" ", FormatArray);
            if (FormatStringArray != null)
                fmtList = string.Join(" ", FormatStringArray);

            return string.Format("{0}={1} {2} {3} {4}", TypeName, MediaType, port, Transport, fmtList);
        }

        public IEnumerator<TypeValue> GetEnumerator()
        {
            return _mediaDescription.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    /// <summary>
    /// The session or media level attribute base class.
    /// </summary>
    /// <remarks>
    /// This class has derived classes. However, currently there can only be one class that
    /// has a TypeName of "a". This parser must create objects from derived Attribute classes.
    /// </remarks>
    public abstract class Attribute : TypeValue
    {
        /// <summary>
        /// The type of this <see cref="TypeValue"/>.
        /// </summary>
        public const string TypeName = "a";

        /// <summary>
        /// Derived <see cref="TypeValue"/> specific line parser called by
        /// <see cref="SessionDescription.Parse(string)"/>.
        /// </summary>
        /// <param name="session">The <see cref="SessionDescription"/> the resulting object
        /// is going to be added to.</param>
        /// <param name="line">The string to be parsed.</param>
        /// <param name="mediaParent">Null or last <see cref="Media"/> object returned while
        /// parsing.</param>
        /// <returns>The parsing result object.</returns>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The parameter <paramref name="line"/> input was not parsed successfully.
        /// </exception>
        public static TypeValue Parse(SessionDescription session, string line, Media mediaParent)
        {
            if (!line.StartsWith(TypeName))
                throw new InvalidSessionDescriptionException("Attribute type parser (a=) called with invalid line = " + line);

            line = line.Substring(2);

            if (line.StartsWith(ControlAttribute.CONTROL))
            {
                Uri url = null;
                if (line.Substring(ControlAttribute.CONTROL.Length) != "*")
                    url = new Uri(line.Substring(ControlAttribute.CONTROL.Length), UriKind.RelativeOrAbsolute);

                return new ControlAttribute(session, TypeName, mediaParent, url);
            }

            if (line.StartsWith(RtpMapAttribute.RTPMAP))
            {
                string[] fields = SpaceSplit(line.Substring(RtpMapAttribute.RTPMAP.Length));
                int payloadType;
                if (!int.TryParse(fields[0], out payloadType))
                    throw new InvalidSessionDescriptionException("Attribute rtpmap: payload type not a valid integer: " + fields[0]);

                string[] encodingSubfields = SlashSplit(fields[1]);
                int clockRate;
                if (!int.TryParse(encodingSubfields[1], out clockRate))
                    throw new InvalidSessionDescriptionException("Attribute rtpmap: clock rate not a valid integer: " + encodingSubfields[1]);
                int encodingParameters = 0;
                if (encodingSubfields.Length > 2)
                {
                    if (!int.TryParse(encodingSubfields[2], out encodingParameters))
                        throw new InvalidSessionDescriptionException("Attribute rtpmap: encoding parameters not a valid integer: " + encodingSubfields[2]);
                }

                return new RtpMapAttribute(session, TypeName, mediaParent, payloadType,
                    encodingSubfields[0], clockRate, encodingParameters);
            }

            if (line.StartsWith(FmtpAttribute.FMTP))
            {
                int spaceIndex = line.IndexOf(' ', FmtpAttribute.FMTP.Length);
                if (spaceIndex < 0)
                    throw new InvalidSessionDescriptionException("Attribute fmtp invalid format");

                int payloadType;
                if (!int.TryParse(line.Substring(FmtpAttribute.FMTP.Length, spaceIndex - FmtpAttribute.FMTP.Length), out payloadType))
                    throw new InvalidSessionDescriptionException("Attribute fmtp: format not a valid integer: " +
                        line.Substring(FmtpAttribute.FMTP.Length, spaceIndex - FmtpAttribute.FMTP.Length));

                return new FmtpAttribute(session, TypeName, mediaParent, payloadType, line.Substring(spaceIndex + 1));
            }

            return new GenericAttribute(session, TypeName, mediaParent, line);
        }

        /// <summary>
        /// Initialize an abstract attribute object.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="type"></param>
        /// <param name="mediaParent"></param>
        protected Attribute(SessionDescription session, string type, Media mediaParent)
            : base(session, type, mediaParent)
        {
        }
    }

    /// <summary>
    /// A session or media level control attribute.
    /// </summary>
    /// <remarks>
    /// This attribute is defined in RFC 2326 (RTSP).
    /// <para>
    /// If used for individual media, it indicates the URL to be used for controlling that
    /// particular media stream.If found at the session level, the attribute indicates the
    /// URL for aggregate control.
    /// </para>
    /// </remarks>
    public class ControlAttribute : Attribute
    {
        /// <summary>
        /// The attribute name of this <see cref="Attribute"/>.
        /// </summary>
        public const string CONTROL = "control:";

        public readonly Uri Url;

        /// <summary>
        /// Create a control attribute <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="type"></param>
        /// <param name="mediaParent"></param>
        /// <param name="url"></param>
        public ControlAttribute(SessionDescription session, string type, Media mediaParent,
                                Uri url)
            : base(session, type, mediaParent)
        {
            Url = url;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            string url = "*";
            if (Url != null)
                url = Url.ToString();

            return string.Format("{0}={1}{2}", TypeName, CONTROL, url);
        }
    }

    /// <summary>
    /// A media level rtpmap attribute.
    /// </summary>
    public class RtpMapAttribute : Attribute
    {
        /// <summary>
        /// The attribute name of this <see cref="Attribute"/>.
        /// </summary>
        public const string RTPMAP = "rtpmap:";

        public readonly int PayloadType;
        public readonly string EncodingName;
        public readonly int ClockRate;
        public readonly int EncodingParameters;

        /// <summary>
        /// Create a rtpmap attribute <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="type"></param>
        /// <param name="mediaParent"></param>
        /// <param name="payloadType"></param>
        /// <param name="encodingName"></param>
        /// <param name="clockRate"></param>
        /// <param name="encodingParameters"></param>
        public RtpMapAttribute(SessionDescription session, string type, Media mediaParent,
                                int payloadType,
                                string encodingName, int clockRate, int encodingParameters)
            : base(session, type, mediaParent)
        {
            PayloadType = payloadType;
            EncodingName = encodingName;
            ClockRate = clockRate;
            EncodingParameters = encodingParameters;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            string encodingParameter = string.Empty;
            if (EncodingParameters != 0)
                encodingParameter = string.Format("/{0}", EncodingParameters);

            return string.Format("{0}={1}{2} {3}/{4}{5}", TypeName, RTPMAP, PayloadType,
                EncodingName, ClockRate, encodingParameter);
        }
    }

    /// <summary>
    /// A media level fmtp attribute.
    /// </summary>
    public class FmtpAttribute : Attribute
    {
        /// <summary>
        /// The attribute name of this <see cref="Attribute"/>.
        /// </summary>
        public const string FMTP = "fmtp:";

        public readonly int PayloadType;
        public readonly string FormatParameters;

        /// <summary>
        /// Create a fmtp attribute <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="type"></param>
        /// <param name="mediaParent"></param>
        /// <param name="payloadType"></param>
        /// <param name="formatParameters"></param>
        public FmtpAttribute(SessionDescription session, string type, Media mediaParent,
                                int payloadType, string formatParameters)
            : base(session, type, mediaParent)
        {
            PayloadType = payloadType;
            FormatParameters = formatParameters;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}{2} {3}", TypeName, FMTP, PayloadType, FormatParameters);
        }
    }

    /// <summary>
    /// Any level string attribute.
    /// </summary>
    public class GenericAttribute : Attribute
    {
        public readonly string Value;

        /// <summary>
        /// Create a generic attribute <see cref="TypeValue"/>.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="type"></param>
        /// <param name="mediaParent"></param>
        /// <param name="value"></param>
        public GenericAttribute(SessionDescription session, string type, Media mediaParent,
                                   string value)
            : base(session, type, mediaParent)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the type=value string representation of this <see cref="TypeValue"/>.
        /// </summary>
        /// <returns>A type=value <see cref="string"/> that can be parsed.</returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", TypeName, Value);
        }
    }
}
