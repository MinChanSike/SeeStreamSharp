﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SeeStreamSharp.SDP
{
    /// <summary>
    /// Provides an implementation of SDP.
    /// </summary>
    /// <remarks>
    /// Implements parsing, validation, and generation of the RFC 4566 Session Description
    /// Protocol (SDP, https://tools.ietf.org/html/rfc4566).
    /// </remarks>
    public class SessionDescription : IEnumerable<TypeValue>
    {
        private const string s_ParseMethod = "Parse";
        private const string s_TypeNameField = "TypeName";

        private static Dictionary<string, Type> s_typeClasses = new Dictionary<string, Type>();

        /// <summary>
        /// Builds the table contained in <see cref="s_typeClasses"/>.
        /// </summary>
        static SessionDescription()
        {
            Assembly a = Assembly.GetExecutingAssembly();
            foreach (Type t in a.GetTypes())
            {
                if (t.IsSubclassOf(typeof(TypeValue)))
                {
                    MemberInfo[] parse = t.GetMember(s_ParseMethod, BindingFlags.Public | BindingFlags.Static);
                    if (parse.Length == 1)
                    {
                        FieldInfo typeName = t.GetField(s_TypeNameField, BindingFlags.Public | BindingFlags.Static | BindingFlags.GetField);
                        if (typeName.IsStatic)
                            s_typeClasses.Add(typeName.GetValue(null) as string, t);
                    }
                }
            }
        }

        /// <summary>
        /// Parse the text of a Session Description to an object representation.
        /// </summary>
        /// <param name="lines">A <see cref="string"/> object containg the lines of text
        /// in the session description.</param>
        /// <returns>The resulting <see cref="SessionDescription"/> object.</returns>
        /// <exception cref="ArgumentException">
        /// The <paramref name="lines"/> parameter is null or empty.
        /// </exception>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The <paramref name="lines"/> parameter contains a line that is invalid.
        /// </exception>
        public static SessionDescription Parse(string lines)
        {
            if (string.IsNullOrEmpty(lines))
                throw new ArgumentException("String is null or empty", "lines");

            TextReader reader = new StringReader(lines);

            SessionDescription sd = new SessionDescription();
            Media currentMediaDescription = null;
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                    break;

                int length = line.IndexOf('=');
                if (length == -1)
                    throw new InvalidSessionDescriptionException("Session description is missing type and value delimiter ('='): " + line);

                string typeName = line.Substring(0, length);

                try
                {
                    Type typeValueClass = s_typeClasses[typeName];

                    object[] args = new object[3] { sd, line, currentMediaDescription };

                    object o = typeValueClass.InvokeMember(s_ParseMethod, BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
                                                           null, null, args);
                    TypeValue stv = o as TypeValue;
                    if (stv == null)
                        throw new InvalidSessionDescriptionException("Failed to create SdpTypeValue object: " + line);

                    sd.AddTypeValue(stv);

                    Media smv = o as Media;
                    if (smv != null)
                        currentMediaDescription = smv;
                }
                catch (KeyNotFoundException)
                {
                    throw new InvalidSessionDescriptionException("Session description line has an unsupperted type: " + line);
                }
            }

            return sd;
        }

        private List<TypeValue> _sessionDescription = new List<TypeValue>();

        private List<Media> _mediaDescriptions = new List<Media>();

        /// <summary>
        /// Gets the count of <see cref="TypeValue"/> objects not belonging to a specific
        /// <see cref="Media"/> parent.
        /// </summary>
        public int SessionDescriptionCount
        {
            get { return _sessionDescription.Count; }
        }

        /// <summary>
        /// Gets the count of <see cref="Media"/> specific objects.
        /// </summary>
        public int MediaDescriptionCount
        {
            get { return _mediaDescriptions.Count; }
        }

        /// <summary>
        /// Gets the enumerator of <see cref="Media"/> objects.
        /// </summary>
        public IEnumerable<Media> MediaDescriptions
        {
            get { return _mediaDescriptions; }
        }

        /// <summary>
        /// Add a <see cref="TypeValue"/> object to this collection.
        /// </summary>
        /// <param name="typeValue"></param>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The <paramref name="typeValue"/> parameter can't be added to this object.
        /// </exception>
        public void AddTypeValue(TypeValue typeValue)
        {
            IsValueTypeAllowedToBeAdded(typeValue);

            if (typeValue.GetType() == typeof(Media))
                _mediaDescriptions.Add(typeValue as Media);
            else
            {
                if (typeValue.MediaParent == null)
                    _sessionDescription.Add(typeValue);
                else
                {
                    if (_mediaDescriptions.Contains(typeValue.MediaParent))
                        typeValue.MediaParent.AddTypeValue(typeValue);
                    else
                        throw new InvalidSessionDescriptionException("TypeValue MediaParent value is unknown");
                }
            }
        }

        /// <summary>
        /// Throw exception if <paramref name="typeValue"/> is NOT permitted to be added in the current state.
        /// </summary>
        /// <param name="typeValue">The <see cref="TypeValue"/> object to validate.</param>
        /// <exception cref="InvalidSessionDescriptionException">
        /// The <paramref name="typeValue"/> can not be added.
        /// </exception>
        private void IsValueTypeAllowedToBeAdded(TypeValue typeValue)
        {
            if (typeValue.GetType() == typeof(Version))
            {
                if (typeValue.MediaParent != null)
                    throw new InvalidSessionDescriptionException("Version type not allowed in media description");
                if (SessionDescriptionCount != 0)
                    throw new InvalidSessionDescriptionException("Version type only allowed first in SessionDescription");

                return;
            }
            else
            {
                if (typeValue.MediaParent == null && SessionDescriptionCount == 0)
                    throw new InvalidSessionDescriptionException("Session description must start with Version type");
            }

            if (typeValue.GetType() == typeof(Origin))
            {
                if (typeValue.MediaParent != null)
                    throw new InvalidSessionDescriptionException("Origin type not allowed in media description");
                if (_sessionDescription.OfType<Origin>().FirstOrDefault() != null)
                    throw new InvalidSessionDescriptionException("Origin type duplicate not allowed");

                return;
            }

            if (typeValue.GetType() == typeof(SessionName))
            {
                if (typeValue.MediaParent != null)
                    throw new InvalidSessionDescriptionException("SessionName type not allowed in media description");
                if (_sessionDescription.OfType<SessionName>().FirstOrDefault() != null)
                    throw new InvalidSessionDescriptionException("SessionName type duplicate not allowed");

                return;
            }

            if (typeValue.GetType() == typeof(SessionAndMediaInformation))
            {
                if (typeValue.MediaParent == null)
                {
                    if (_sessionDescription.OfType<SessionAndMediaInformation>().FirstOrDefault() != null)
                        throw new InvalidSessionDescriptionException("SessionAndMediaInformation type duplicate in session level description not allowed");
                }
                else
                {
                    if (typeValue.MediaParent.OfType<SessionAndMediaInformation>().FirstOrDefault() != null)
                        throw new InvalidSessionDescriptionException("SessionAndMediaInformation type duplicate in media level description not allowed");
                }

                return;
            }

            if (typeValue.GetType() == typeof(Url))
            {
                if (typeValue.MediaParent == null)
                {
                    if (_sessionDescription.OfType<Url>().FirstOrDefault() != null)
                        throw new InvalidSessionDescriptionException("Uri type duplicate in session level description not allowed");
                }
                else
                    throw new InvalidSessionDescriptionException("Uri type in media level description not allowed");

                return;
            }

            if (typeValue.GetType() == typeof(Email))
            {
                if (typeValue.MediaParent != null)
                    throw new InvalidSessionDescriptionException("Email type not allowed in media level description");

                return;
            }

            if (typeValue.GetType() == typeof(Phone))
            {
                if (typeValue.MediaParent != null)
                    throw new InvalidSessionDescriptionException("Phone type not allowed in media level description");

                return;
            }

            if (typeValue.GetType() == typeof(Connection))
            {
                if (typeValue.MediaParent == null)
                {
                    if (_sessionDescription.OfType<Connection>().FirstOrDefault() != null)
                        throw new InvalidSessionDescriptionException("Connection type duplicate in session level description not allowed");
                }
                else
                {
                    if (typeValue.MediaParent.OfType<Connection>().FirstOrDefault() != null)
                        throw new InvalidSessionDescriptionException("Connection type duplicate in media level description not allowed");
                }

                return;
            }
        }

        /// <summary>
        /// Returns a session level <see cref="TypeValue"/> enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TypeValue> SessionLevelTypeValues()
        {
            //  Protect the List so it can't be modified by external code.
            return _sessionDescription.AsReadOnly();
        }

        /// <summary>
        /// The types of media supported by SDP.
        /// </summary>
        public enum MediaType
        {
            UNDEFINED,
            audio,
            video,
            application,
            data,
            control,
            FIRST_DYNAMIC
        }

        private Dictionary<string, int> AuxiliaryMediaType;

        /// <summary>
        /// Convert a text media type to a <see cref="MediaType"/> value.
        /// </summary>
        /// <param name="input">The <see cref="string"/> to convert.</param>
        /// <returns>The resulting <see cref="MediaType"/> value.</returns>
        public MediaType ParseMediaType(string input)
        {
            MediaType mt = MediaType.UNDEFINED;

            try
            {
                mt = (MediaType)Enum.Parse(typeof(MediaType), input, true);
            }
            catch (ArgumentException)
            {
                if (AuxiliaryMediaType == null)
                    AuxiliaryMediaType = new Dictionary<string, int>();

                int value;
                if (!AuxiliaryMediaType.TryGetValue(input, out value))
                {
                    value = AuxiliaryMediaType.Count + 1 + (int)MediaType.FIRST_DYNAMIC;
                    AuxiliaryMediaType.Add(input, value);
                }
                mt = (MediaType)(value);
            }

            return mt;
        }

        /// <summary>
        /// Convert a <see cref="MediaType"/> value to a text media type.
        /// </summary>
        /// <param name="mt">The <see cref="MediaType"/> value to convert.</param>
        /// <returns>The <see cref="string"/> value.</returns>
        public string ToString(MediaType mt)
        {
            if (mt > MediaType.FIRST_DYNAMIC)
            {
                int value = (int)mt;

                foreach (KeyValuePair<string, int> item in AuxiliaryMediaType)
                {
                    if (item.Value == value)
                        return item.Key;
                }
            }

            return null;
        }

        #region IEnumerable<TypeValue>

        /// <summary>
        /// Returns an enumerator that iterates through all <see cref="TypeValue"/> in this
        /// session descriptor.
        /// </summary>
        /// <returns>An <see cref="IEnumerator{TypeValue}"/>.</returns>
        public IEnumerator<TypeValue> GetEnumerator()
        {
            foreach (TypeValue tv in _sessionDescription)
                yield return tv;
            foreach (Media media in _mediaDescriptions)
            {
                yield return media;
                foreach (TypeValue tv in media)
                    yield return tv;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
