﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.RTP;
using System;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// MP4V-ES RTP payload to Access Unit buffer converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 3016 RTP Payload Format for
    /// MPEG-4 Audio/Visual Streams (https://tools.ietf.org/html/rfc3016).
    /// </remarks>
    internal sealed class Mp4vEsFrameBuffer : AbstractFrameBuffer
    {
        private const byte VISUAL_OBJECT_SEQUENCE_START_CODE = 0xB0;

        public Mp4vEsFrameBuffer()
            : base(LogManager.GetLogger<Mp4vEsFrameBuffer>())
        {
        }

        protected override void DoNext(Packet value)
        {
            //  MP4V-ES payload requires a non-padded byte count.
            value.Header.RemovePadding(ref value.Payload);

            if (value.Payload.Count < 1)
                return;

            byte[] buffer = new byte[value.Payload.Count];
            Array.Copy(value.Payload.Array, value.Payload.Offset,
                       buffer, 0,
                       value.Payload.Count);

            bool syncFrame = false;

            //  The RFC states that "the RTP payload SHALL begin with the header of the
            //  syntactically highest function."

            //  ISO/IEC 14496:2 states that the start code prefix is a specific bit
            //  pattern that do not otherwise occur in the video stream.
            if (value.Payload.Count > 3 && (value.Payload.Array[0] == 0 &
                                            value.Payload.Array[1] == 0 &
                                            value.Payload.Array[2] == 1))
            {
                if (value.Payload.Array[3] == VISUAL_OBJECT_SEQUENCE_START_CODE)
                    syncFrame = true;
            }

            FragmentFlags flags = value.Header.Marker ? FragmentFlags.Marker : 0;
            flags |= syncFrame ? FragmentFlags.SyncFrame : 0;

            ConsecutiveSequenceBasedAdd(value.Header.Sequence, value.Header.TimeStamp, flags, buffer);
        }
    }
}
