﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.RTP;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace SeeStreamSharp.Media.RtpStreamSink
{
    /// <summary>
    /// H.264 RTP payload to NAL unit buffer converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 3984 RTP Payload Format for
    /// H.264 Video (https://tools.ietf.org/html/rfc3984).
    /// </remarks>
    internal sealed class H264FrameBuffer : AbstractFrameBuffer
    {
        internal const int NAL_UNKNOWN = 0;
        internal const int NAL_SLICE = 1;
        internal const int NAL_SLICE_DPA = 2;
        internal const int NAL_SLICE_DPB = 3;
        internal const int NAL_SLICE_DPC = 4;
        internal const int NAL_SLICE_IDR = 5;       /* ref_idc != 0 */
        internal const int NAL_SEI = 6;             /* ref_idc == 0 */
        internal const int NAL_SPS = 7;
        internal const int NAL_PPS = 8;
        internal const int NAL_AU_DELIMITER = 9;    /* ref_idc == 0 */
        internal const int NAL_END_OF_SEQ = 10;     /* ref_idc == 0 */
        internal const int NAL_END_OF_STREAM = 11;  /* ref_idc == 0 */
        internal const int NAL_FILLER_DATA = 12;    /* ref_idc == 0 */
        internal const int NAL_SPS_EXT = 13;
        internal const int NAL_PREFIX = 14;
        internal const int NAL_SUBSET_SPS = 15;
        internal const int NAL_DEPTH_PS = 16;
        internal const int NAL_RESERVED_17 = 17;
        internal const int NAL_RESERVED_18 = 18;
        internal const int NAL_SLICE_WP = 19;
        internal const int NAL_SLICE_EXT = 20;
        internal const int NAL_SLICE_3D_EXT = 21;
        internal const int NAL_RESERVED_22 = 22;
        internal const int NAL_RESERVED_23 = 23;

        internal const int NAL_RESERVED_30 = 30;
        internal const int NAL_RESERVED_31 = 31;

        //  RFC 3984
        internal const int NAL_STAP_A = 24;
        internal const int NAL_STAP_B = 25;
        internal const int NAL_MTAP16 = 26;
        internal const int NAL_MTAP24 = 27;
        internal const int NAL_FU_A = 28;
        internal const int NAL_FU_B = 29;

        //  NAL unit octet.
        internal static readonly BitVector32.Section s_nalType = BitVector32.CreateSection(31);
        private static readonly BitVector32.Section s_nalRefIdc = BitVector32.CreateSection(3, s_nalType);
        private static readonly BitVector32.Section s_forbiddenZero = BitVector32.CreateSection(1, s_nalRefIdc);

        //  NAL FU indicator.
        private static readonly BitVector32.Section s_fuReserved = BitVector32.CreateSection(1, s_nalType);
        private static readonly BitVector32.Section s_fuEnd = BitVector32.CreateSection(1, s_fuReserved);
        private static readonly BitVector32.Section s_fuStart = BitVector32.CreateSection(1, s_fuEnd);

        private IH264Consumer _H264Consumer;

        public H264FrameBuffer()
            : base(LogManager.GetLogger<H264FrameBuffer>())
        {
        }

        public override void Connect(IFrameBufferConsumer consumer, Session session)
        {
            base.Connect(consumer, session);
            _H264Consumer = consumer as IH264Consumer;
        }

        protected override void DoNext(Packet value)
        {
            //  H.264 payload requires a non-padded byte count.
            value.Header.RemovePadding(ref value.Payload);

            if (value.Payload.Count < 1)
                return;

            FragmentFlags flags = 0;
            byte[] buffer = null;
            BitVector32 nalUnitType = new BitVector32(value.Payload.Array[value.Payload.Offset]);
            bool syncFrame = false;

            switch (nalUnitType[s_nalType])
            {
                //  NAL types not specified
                case NAL_UNKNOWN:
                case NAL_RESERVED_30:
                case NAL_RESERVED_31:
                    _logger.WarnFormat("{0} Undefined NAL types.", _loggingId);
                    break;

                //  This implementation does not support Interleaved mode (2).
                case NAL_STAP_B:
                case NAL_MTAP16:
                case NAL_MTAP24:
                case NAL_FU_B:
                    _logger.WarnFormat("{0} Interleaved mode (2) not supported.", _loggingId);
                    break;

                case NAL_STAP_A:
                    buffer = ParseStap(value.Payload.Array,
                                       value.Payload.Offset + 1, value.Payload.Count - 1,
                                       ref syncFrame);
                    break;

                case NAL_FU_A:
                    // For this NALUs, the first two bytes are the FU indicator and the FU header.
                    if (value.Payload.Count < 2)
                        return;

                    BitVector32 fuHeader = new BitVector32(value.Payload.Array[value.Payload.Offset + 1]);
                    flags = FragmentFlags.FragmentedNal;
                    flags |= fuHeader[s_fuEnd] == 1 ? FragmentFlags.EndOfNal : 0;

                    // If the start bit is not set we just skip the FU indicator and header.
                    int bytesToRemove = 2;
                    bool startBit = fuHeader[s_fuStart] == 1;
                    if (startBit)
                    {
                        flags |= FragmentFlags.StartOfNalFragment;
                        nalUnitType[s_nalType] = fuHeader[s_nalType];
                        bytesToRemove = 1;
                    }

                    buffer = new byte[value.Payload.Count - bytesToRemove];
                    Array.Copy(value.Payload.Array, value.Payload.Offset + bytesToRemove,
                               buffer, 0,
                               value.Payload.Count - bytesToRemove);

                    if (startBit)
                        buffer[0] = (byte)nalUnitType.Data;
                    break;

                case NAL_SPS:
                case NAL_PPS:
                    //  These are not stored in the element stream. They have to be added
                    //  to the sample description box.
                    _H264Consumer.AddToParameterSets(value.Payload.Array, value.Payload.Offset, value.Payload.Count);
                    break;

                //  Filler data is not to be stored in the media container according to
                //  ISO 14496-15:2004.
                case NAL_FILLER_DATA:
                //  These are not to be included in the container (and only specified as
                //  reserved) according to ISO 14496-15:2004.
                case NAL_SPS_EXT:
                case NAL_PREFIX:
                case NAL_SUBSET_SPS:
                case NAL_DEPTH_PS:
                case NAL_RESERVED_17:
                case NAL_RESERVED_18:
                case NAL_SLICE_WP:
                case NAL_SLICE_EXT:
                case NAL_SLICE_3D_EXT:
                case NAL_RESERVED_22:
                case NAL_RESERVED_23:
                    break;

                default:
                    buffer = new byte[value.Payload.Count];
                    Array.Copy(value.Payload.Array, value.Payload.Offset, buffer, 0, value.Payload.Count);
                    flags = FragmentFlags.EndOfNal;
                    break;
            }

            flags |= value.Header.Marker ? FragmentFlags.Marker : 0;
            flags |= (nalUnitType[s_nalType] == NAL_SLICE_IDR || syncFrame) ? FragmentFlags.SyncFrame : 0;

            ConsecutiveSequenceBasedAdd(value.Header.Sequence, value.Header.TimeStamp, flags, buffer);
        }

        private byte[] ParseStap(byte[] buffer, int offset, int count, ref bool syncFrame)
        {
            BinaryWriter writer = new BinaryWriter(new MemoryStream(count));
            int index = 0;

            while (count > 1)
            {
                index++;
                int length = (buffer[offset] << 8) + buffer[offset + 1];
                offset += 2;
                count -= 2;
                if (length > count)
                {
                    _logger.ErrorFormat("{0} STAP content lenght={1} but remaining bytes={2}",
                        _loggingId, length, count);
                    break;
                }

                BitVector32 nalUnitType = new BitVector32(buffer[offset]);
                int nalType = nalUnitType[s_nalType];

                _logger.TraceFormat("{0} STAP content{1}={2}({3})",
                    _loggingId, index, nalType, length);

                switch (nalType)
                {
                    case NAL_SPS:
                    case NAL_PPS:
                        _H264Consumer.AddToParameterSets(buffer, offset, length);
                        break;
                    //  Filler data is not to be stored in the media container according to
                    //  ISO 14496-15:2004.
                    case NAL_FILLER_DATA:
                    //  These are not to be included in the container (and only specified as
                    //  reserved) according to ISO 14496-15:2004.
                    case NAL_SPS_EXT:
                    case NAL_PREFIX:
                    case NAL_SUBSET_SPS:
                    case NAL_DEPTH_PS:
                    case NAL_RESERVED_17:
                    case NAL_RESERVED_18:
                    case NAL_SLICE_WP:
                    case NAL_SLICE_EXT:
                    case NAL_SLICE_3D_EXT:
                    case NAL_RESERVED_22:
                    case NAL_RESERVED_23:
                        break;
                    default:
                        writer.Write(IPAddress.HostToNetworkOrder(length));
                        writer.Write(buffer, offset, length);
                        break;
                }
                if (nalType == NAL_SLICE_IDR)
                    syncFrame = true;

                offset += length;
                count -= length;
            }

            if (count != 0)
                _logger.ErrorFormat("{0} STAP remaining content lenght={1}", _loggingId, count);

            count = (int)writer.BaseStream.Length;
            byte[] result = new byte[count];
            writer.Seek(0, SeekOrigin.Begin);
            writer.BaseStream.Read(result, 0, count);
            return result;
        }

        private int _fragmentSize;
        private int _fragmentSizeIndex = -1;

        public override int ProcessFragmentConcatenation(IList<ArraySegment<byte>> content, int index)
        {
            Fragment fragment = _fragments[index];

            //  Don't add an invalid or excluded fragment.
            if (fragment.Payload == null)
                return -1;

            int bytesAdded = 0;
            int nalSize = fragment.Payload.Length;
            bool addBeforeThisFragment = true;

            if ((fragment.Flags & FragmentFlags.FragmentedNal) == FragmentFlags.FragmentedNal)
            {
                if ((fragment.Flags & FragmentFlags.StartOfNalFragment) == FragmentFlags.StartOfNalFragment)
                {
                    _fragmentSize = 0;
                    _fragmentSizeIndex = content.Count;
                }

                _fragmentSize += fragment.Payload.Length;

                if ((fragment.Flags & FragmentFlags.EndOfNal) == FragmentFlags.EndOfNal)
                {
                    nalSize = _fragmentSize;
                    addBeforeThisFragment = false;
                }
            }

            if ((fragment.Flags & FragmentFlags.EndOfNal) == FragmentFlags.EndOfNal)
            {
                byte[] fragmentSize = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(nalSize));
                if (addBeforeThisFragment)
                    content.Add(new ArraySegment<byte>(fragmentSize));
                else
                {
                    content.Insert(_fragmentSizeIndex, new ArraySegment<byte>(fragmentSize));
                    _fragmentSizeIndex = -1;
                }
                bytesAdded = 4;
            }

            return bytesAdded;
        }
    }
}
