﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.RTP;
using System;

namespace SeeStreamSharp.Media.RtpStreamSink
{
    /// <summary>
    /// MPEG-4 AAC_HBR RTP payload to Access Unit buffer converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 3640 RTP Payload Format for
    /// Transport of MPEG-4 Elementary Streams (https://tools.ietf.org/html/rfc3640).
    /// </remarks>
    internal sealed class Mpeg4AacHbrFrameBuffer : AbstractFrameBuffer
    {
        private const int AU_HEADER_LENGTH_BYTES = 2;

        private int _constantDuration;

        public Mpeg4AacHbrFrameBuffer(int constantDuration)
            : base(LogManager.GetLogger<Mpeg4AacHbrFrameBuffer>())
        {
            _constantDuration = constantDuration;
        }

        protected override void DoNext(Packet value)
        {
            //  RFC: A packet carrying AAC-hbr always begins with AU-headers-length and
            //  the associated AU-header(s). It has a fixed sizeLength == 13 and
            //  indexlength/indexdeltalength == 3 making it a total of 2 bytes.
            //  The payload is always one or more complete AU's.
            if (value.Payload.Count < AU_HEADER_LENGTH_BYTES)
            {
                _logger.Warn("Invalid payload byte count");
                return;
            }

            //  Get the AU-header-lenght in bits.
            int auHeaderSize = (value.Payload.Array[value.Payload.Offset] << 8) +
                value.Payload.Array[value.Payload.Offset + 1];
            if ((auHeaderSize & 15) != 0)
            {
                _logger.Warn("Invalid AU-header-lenght value");
                return;
            }
            //  Now the header size is recalculated to the count of 16 bit words.
            auHeaderSize >>= 4;

            //  One RTSP server, or at least its Session Descriptor builder, "forgets"
            //  to add the "constantDuration=" parameter. Try to calculate it to be able
            //  to produce a fully valid AU sequence.
            if (auHeaderSize > 1 & _constantDuration == 0)
                _constantDuration = TryCalculateConstantDuration();

            //  The total size of the AU Header section in bytes.
            int auOffset = AU_HEADER_LENGTH_BYTES + auHeaderSize * 2;

            if (value.Payload.Count < auOffset)
            {
                _logger.Warn("AU-header-length size overflow");
                return;
            }

            //  The assumption is that concatenated AU's may be carried in the RTP packet.
            //  However, it is hard coded to only handle a maximum of 8 AU's per RTP
            //  packet.
            if (auHeaderSize > 8)
            {
                _logger.Error("Hardcoded max AU count overflow");
                auHeaderSize = 8;
            }
            int shiftedSequenceNumber = value.Header.Sequence << 3;

            int auHeaderOffset = AU_HEADER_LENGTH_BYTES;
            for (int i = 0; i < auHeaderSize; i++)
            {
                int auHeader = value.Payload.Array[value.Payload.Offset + auHeaderOffset];
                auHeaderOffset++;
                auHeader <<= 8;
                auHeader += value.Payload.Array[value.Payload.Offset + auHeaderOffset];
                auHeaderOffset++;

                int bytes = auHeader >> 3;
                int index = auHeader & 7;
                if (auOffset + bytes > value.Payload.Count)
                {
                    _logger.ErrorFormat("AU at offset {0} has size {1} but packet size is only {2}",
                        auOffset, bytes, value.Payload.Count);

                    //  This condition will throw an exception in the Array.Copy() below.
                    //  However, the exception would just end up in the "lower level"
                    //  network processing code so just return...
                    return;
                }

                byte[] buffer = new byte[bytes];
                Array.Copy(value.Payload.Array, value.Payload.Offset + auOffset,
                           buffer, 0,
                           bytes);
                auOffset += bytes;

                FragmentFlags flags = value.Header.Marker ? FragmentFlags.Marker : 0;

                SortedNonConsecutiveAdd(shiftedSequenceNumber + i,
                    value.Header.TimeStamp + i * _constantDuration, flags, buffer);
            }
        }
    }
}
