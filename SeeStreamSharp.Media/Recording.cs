﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.SessionDescriptionSetup;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeeStreamSharp.Media
{
    /// <summary>
    /// Provides recording capabilities given a <see cref="IAsyncStreamingClient"/> and a
    /// file name.
    /// </summary>
    /// <remarks>
    /// To enable external code to start and stop a recording in the background, and react
    /// to the result when it becomes available, all methods are asynchronous.
    /// </remarks>
    public sealed class Recording
    {
        private readonly ILog _logger = LogManager.GetLogger<Recording>();
        private readonly int _loggingId;

        /// <summary>
        /// The list of Tasks setting up each stream.
        /// </summary>
        /// <remarks>
        /// It is expected to only hold 2 items.
        /// <para>
        /// key = Task<Session>, value = IStreamSinkFactory
        /// </para>
        /// </remarks>
        private readonly ListDictionary _sessions = new ListDictionary();
        private readonly IAsyncStreamingClient _streamingClient;
        private readonly string _mediaFileName;
        private Task _startTask;
        private Exception _exception;

        public Exception Exception { get { return _exception; } }

        public IContainerInfo MediaContainer { get; private set; }

        /// <summary>
        /// Create a recording object using a RTSP presentation and media container file name.
        /// </summary>
        /// <param name="streamingClient">The <see cref="IAsyncStreamingClient"/> whos media
        /// streams are to be recorded.</param>
        /// <param name="mediaFileName">The file name excluding extension to store the
        /// presentation in.</param>
        public Recording(IAsyncStreamingClient streamingClient, string mediaFileName)
        {
            if (streamingClient as ISessionDescriptionSetup == null)
                throw new ArgumentException("Only ISessionDescriptionSetup streaming clients are currently supported", "streamingClient");
            if (string.IsNullOrEmpty(mediaFileName))
                throw new ArgumentException("Is null or empty", "mediaFileName");

            _streamingClient = streamingClient;
            _mediaFileName = mediaFileName;

            _loggingId = RuntimeHelpers.GetHashCode(this);

            _logger.InfoFormat("[{0}] created, file name={1}", _loggingId, mediaFileName);
        }

        /// <summary>
        /// Start a request to open the <see cref="IAsyncStreamingClient"/> and create the
        /// media container.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        public Task StartAsync()
        {
            _logger.TraceFormat("[{0}] StartAsync() called", _loggingId);

            (_streamingClient as ISessionDescriptionSetup)
                .GetSessionDescriptionAsync().ContinueWith(ProcessSessionDescription);
            _startTask = new Task(ProcessPlayResult);

            _logger.TraceFormat("[{0}] StartAsync() returning", _loggingId);

            return _startTask;
        }

        private void ProcessPlayResult()
        {
            //  This recording was not started correctly. If the container was
            //  created it should be closed again.
            if (_exception != null)
            {
                IDisposable disposable = MediaContainer as IDisposable;
                MediaContainer = null;
                if (disposable != null)
                    disposable.Dispose();
                throw _exception;
            }
        }

        /// <summary>
        /// Continuation to run when <see cref="SessionDescription"/> is available.
        /// </summary>
        /// <remarks>
        /// This method can not just throw an exception. Any exception is to be set on the
        /// TaskCompletionSource used to give a Task to the external code.
        /// </remarks>
        /// <param name="taskResult"></param>
        private void ProcessSessionDescription(Task<SessionDescription> taskResult)
        {
            _logger.TraceFormat("[{0}] ProcessSessionDescription() entered", _loggingId);

            try
            {
                //  This will trigger any possible exception getting the SessionDescription.
                SessionDescription sd = taskResult.Result;

                _logger.Debug(fmh => {
                    StringBuilder sb = new StringBuilder();
                    foreach (TypeValue tv in sd)
                        sb.AppendLine(tv.ToString());
                    fmh("[{0}] ProcessSessionDescription() input:{1}{2}",
                        _loggingId, Environment.NewLine, sb);
                });

                //  Key = rtpMap, IStreamSinkFactory
                ListDictionary streams;
                Type container = StreamSinkFactoryFactory
                    .Instance.Create(_streamingClient as ISessionDescriptionSetup, out streams);

                if (container == null)
                    throw new NotSupportedException("No supported media streams");

                MediaContainer = Activator.CreateInstance(container, _mediaFileName) as IContainerInfo;

                //  After setting up all of the streams the StartPlay() call will start
                //  retrieving the media data.
                //  There is no need to occupy a thread on a call to Task.WaitAll()
                //  just waiting for all of the setup tasks to complete. It is a valid
                //  "assumption" to call StartPlay() in a continuation on the session
                //  processing continuation of the last setup task.

                Task lastSetupContinuation = null;

                foreach (DictionaryEntry item in streams)
                {
                    TypeValue tv = item.Key as TypeValue;
                    IStreamSinkFactory ssf = item.Value as IStreamSinkFactory;
                    if (tv != null && ssf != null)
                    {
                        Task<Session> stream = (_streamingClient as ISessionDescriptionSetup)
                            .SetupAsync(tv);
                        _sessions.Add(stream, ssf);
                        lastSetupContinuation = stream.ContinueWith(SetupContinuation);
                    }
                }

                lastSetupContinuation.ContinueWith((setupTask) => {
                    //  Any setup exception is caught in SetupContinuation(). The exception
                    //  is propagated to the task returned by the call to StartAsync().
                    //  This is OK as this task is able to test for an exception the same way
                    //  as the task returned by StartAsync() and discover that it is not to
                    //  start PlayAsync().This would orphan this continuation task but that
                    //  is also OK as it can never by faulted.
                    if (_exception == null)
                    {
                        Task t = _streamingClient.PlayAsync();
                        t.ContinueWith(PlayContinuation);
                    }
                });
            }
            catch (Exception ex)
            {
                Interlocked.CompareExchange(ref _exception, ex, null);
                _startTask.Start();
            }

            _logger.TraceFormat("[{0}] ProcessSessionDescription() exiting", _loggingId);
        }

        /// <summary>
        /// Delegate to run when a stream setup task result is available.
        /// </summary>
        /// <param name="taskResult"></param>
        /// <remarks>
        /// This method can not just throw an exception. Any exception is to be set on the
        /// Task given to the external code.
        /// </remarks>
        private void SetupContinuation(Task<Session> taskResult)
        {
            _logger.TraceFormat("[{0}] SetupContinuation() entered", _loggingId);

            try
            {
                IStreamSinkFactory ssf = _sessions[taskResult] as IStreamSinkFactory;
                if (ssf == null)
                    throw new InvalidOperationException("Unexpected null IStreamSinkFactory");
                ssf.ConnectStreamSink(MediaContainer, taskResult.Result);
            }
            catch (Exception ex)
            {
                //  Only the failure of the first of multiple setup results can be
                //  reported to the external code.
                if (_startTask.Status == TaskStatus.Created)
                {
                    Interlocked.CompareExchange(ref _exception, ex, null);
                    _startTask.Start();
                }
            }

            _logger.TraceFormat("[{0}] SetupContinuation() exiting", _loggingId);
        }

        /// <summary>
        /// Delegate to run when the play task result is available.
        /// </summary>
        /// <param name="playTask"></param>
        /// <remarks>
        /// This method can not just throw an exception. Any exception is to be set on the
        /// Task given to the external code.
        /// </remarks>
        private void PlayContinuation(Task playTask)
        {
            _logger.TraceFormat("[{0}] PlayContinuation() entered", _loggingId);

            if (playTask.IsFaulted)
                Interlocked.CompareExchange(ref _exception, playTask.Exception, null);

            //  If any of the setups failed we would never call StartPlay() so the
            //  _startTask should not have been started previously which would cause
            //  an exception here.
            _startTask.Start();

            _logger.TraceFormat("[{0}] PlayContinuation() exiting", _loggingId);
        }

        /// <summary>
        /// Start a request to stop the <see cref="IAsyncStreamingClient"/> and finalize
        /// the media container.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will signal the result of this request.</returns>
        public Task StopAsync()
        {
            _logger.TraceFormat("[{0}] StopAsync() called", _loggingId);

            if (_startTask == null)
                throw new InvalidOperationException("Start not called");
            if (MediaContainer == null)
                throw new InvalidOperationException("Recording not started");

            Task finalized = _streamingClient.StopAsync().ContinueWith(StopContinuation);

            _logger.TraceFormat("[{0}] StopAsync() returning", _loggingId);

            return finalized;
        }

        private void StopContinuation(Task teardown)
        {
            _logger.TraceFormat("[{0}] StopContinuation() entered", _loggingId);

            //  Regardless of any stream client stop task failure the container has to
            //  be closed.
            (MediaContainer as IDisposable).Dispose();

            _logger.TraceFormat("[{0}] StopContinuation() exiting", _loggingId);
        }
    }
}
