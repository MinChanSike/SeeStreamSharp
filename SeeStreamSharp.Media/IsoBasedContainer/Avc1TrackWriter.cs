﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    /// <summary>
    /// AVC1 track writer.
    /// </summary>
    /// <remarks>
    /// The input is written to the media file as documented in 14496-15:2004.
    /// <para>
    /// This class does not use any of the new box types described in 14496-15:2004. The
    /// major brand in the container does not have to be set to AVC1!
    /// </para>
    /// </remarks>
    internal class Avc1TrackWriter : VideoTrackWriter, IH264Consumer
    {
        private const uint AVC1 = 0x61766331;
        private const uint AVCC = 0x61766343;   //  "avcC"

        private readonly byte[] _profileLevelId;
        private readonly List<byte[]> _parameterSets;

        public Avc1TrackWriter(object container, int clockRate,
            byte[] profileLevelId, List<byte[]> parameterSets,
            AbstractFrameBuffer source)
            : base(LogManager.GetLogger<Avc1TrackWriter>(), container, source, clockRate, AVC1)
        {
            if (parameterSets == null)
                throw new ArgumentNullException("parameterSets");
            if (profileLevelId.Length < 3)
                throw new ArgumentException("Requires to contain 3 bytes", "profileLevelId");

            _profileLevelId = profileLevelId;
            _parameterSets = parameterSets;
        }

        public void AddToParameterSets(byte[] buffer, int offset, int count)
        {
            for (int index = 0; index < _parameterSets.Count; index++)
            {
                byte[] set = _parameterSets[index];
                if (set.Length == count)
                {
                    bool equal = true;

                    while (count > 0)
                    {
                        count--;
                        if (set[count] != buffer[offset + count])
                        {
                            equal = false;
                            break;
                        }
                    }

                    if (equal)
                        return;
                }
            }

            byte[] parameterSet = new byte[count];
            Array.Copy(buffer, offset, parameterSet, 0, count);
            _parameterSets.Add(parameterSet);
        }

        protected override void AddDescriptorBox()
        {
            _cw.AddBox(this, AVCC, AvccAction);
        }

        private void AvccAction()
        {
            _cw.Write((byte)1);             //  Configuration version.
            _cw.Write(_profileLevelId[0]);  //  Profile
            _cw.Write(_profileLevelId[1]);  //  Profile compat
            _cw.Write(_profileLevelId[2]);  //  Level
            //  LengthSize is the number of bytes minus 1 of the individual NALU size header
            //  in the track samples. This is hard coded to a size of 4 bytes making this have
            //  a lengthsize = 3.
            _cw.Write((byte)0xff);          //  Reserved 11111100b | lengthsize = 0x11

            byte count = CountParameterSets(H264FrameBuffer.NAL_SPS);
            _cw.Write((byte)(0xe0 | count));//  Reserved 11100000b | sps count (max 31)
            if (count > 0)
                AddParameterSets(H264FrameBuffer.NAL_SPS);

            count = CountParameterSets(H264FrameBuffer.NAL_PPS);
            _cw.Write(count);
            if (count > 0)
                AddParameterSets(H264FrameBuffer.NAL_PPS);
        }

        private byte CountParameterSets(int nalType)
        {
            byte count = 0;

            for (int index = 0; index < _parameterSets.Count; index++)
            {
                BitVector32 nalUnitType = new BitVector32(_parameterSets[index][0]);
                if (nalUnitType[H264FrameBuffer.s_nalType] == nalType)
                    count++;
            }

            return count;
        }

        private void AddParameterSets(int nalType)
        {
            for (int index = 0; index < _parameterSets.Count; index++)
            {
                byte[] set = _parameterSets[index];
                BitVector32 nalUnitType = new BitVector32(set[0]);
                if (nalUnitType[H264FrameBuffer.s_nalType] == nalType)
                {
                    _cw.WriteHalfWord(set.Length);
                    _cw.Write(set, set.Length);
                }
            }
        }
    }
}
