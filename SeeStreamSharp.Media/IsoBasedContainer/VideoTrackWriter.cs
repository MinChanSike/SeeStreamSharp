﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using System.Collections.Generic;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    internal abstract class VideoTrackWriter : TrackWriter
    {
        private const uint VIDE = 0x76696465;
        private const uint VMHD = 0x766d6864;
        private const string description = "Video Media Handler";
        private const int FIXED_VMHD_FLAG = 1;

        private readonly uint _videoType;

        protected VideoTrackWriter(ILog logger, object container, AbstractFrameBuffer source,
            int clockRate, uint videoType)
            : base(logger, container, source, clockRate, 0, VIDE, description)
        {
            _videoType = videoType;
        }

        /// <summary>
        /// Video track type chunk content calculation.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="lastAddedIndex"></param>
        /// <returns>
        /// If less than Zero it is not yet a suitable chunk to write. If higher than Zero
        /// it is the number of entries in <paramref name="list"/> that is suitable to write
        /// as a chunk to the media container.
        /// </returns>
        /// <remarks>
        /// In a video track the sync frame, AKA a "key frame" or "Instantaneous Decoding
        /// Refresh" when such are used by a codec, are always required to be retrieved when
        /// seeking in the media stream. Dividing chunks at a sync frame, having it become
        /// the first in the next chunk, is therefore considered a very suitable solution.
        /// </remarks>
        protected sealed override int IsChunkReady(IList<AbstractFrameBuffer.Fragment> list, int lastAddedIndex)
        {
            int endCount = 0;

            if (lastAddedIndex > 0)
            {
                AbstractFrameBuffer.Fragment first = list[0];
                AbstractFrameBuffer.Fragment last = list[lastAddedIndex];

                if (first.Timestamp != last.Timestamp)
                {
                    bool sync = (last.Flags & FragmentFlags.SyncFrame) == FragmentFlags.SyncFrame;

                    while (!sync && lastAddedIndex > 0)
                    {
                        lastAddedIndex--;

                        AbstractFrameBuffer.Fragment item = list[lastAddedIndex];
                        if (last.Timestamp != item.Timestamp)
                            break;

                        sync |= (item.Flags & FragmentFlags.SyncFrame) == FragmentFlags.SyncFrame;
                    }

                    if (sync)
                    {
                        while (lastAddedIndex > 0)
                        {
                            AbstractFrameBuffer.Fragment item = list[lastAddedIndex - 1];
                            if (last.Timestamp != item.Timestamp)
                            {
                                endCount = lastAddedIndex;
                                break;
                            }

                            lastAddedIndex--;
                        }
                    }
                }
            }

            return endCount;
        }

        protected override sealed void MediaHeader()
        {
            _cw.AddBox(this, VMHD, VmhdAction);
        }

        private void VmhdAction()
        {
            _cw.Write(0, FIXED_VMHD_FLAG);

            _cw.Write(0);     //  2 byte Graphics mode + 3 * 2 byte Opcolor.
            _cw.Write(0);
        }

        protected sealed override void AddSampleDescriptionBox()
        {
            _cw.AddBox(this, _videoType, VideoDescriptorAction);
        }

        protected abstract void AddDescriptorBox();

        private void VideoDescriptorAction()
        {
            //  General sample description fields:
            _cw.Write(0);                   //  Reserved
            _cw.WriteHalfWord(0);           //  Reserved
            _cw.WriteHalfWord(FIXED_SINGLE_DATA_REF_INDEX);
            //  Video sample description fields:
            _cw.Write(0);                   //  Version + revision level
            _cw.Write(0);                   //  Vendor
            _cw.Write(0);                   //  Temporal quality
            _cw.Write(0);                   //  Spatial quality
            _cw.WriteHalfWord(_frameWidth);
            _cw.WriteHalfWord(_frameHeight);
            _cw.Write(0x00480000);          //  Horizontal resolution: 72 dpi
            _cw.Write(0x00480000);          //  Vertical resolution: 72 dpi
            _cw.Write(0);                   //  Reserved
            _cw.WriteHalfWord(1);           //  Frame count (1 frame per sample.)
            _cw.Write(0);                   //  Compressor name (start).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (continued).
            _cw.Write(0);                   //  Compressor name (final)
            _cw.WriteHalfWord(0x18);        //  Depth.
            _cw.WriteHalfWord(0xffff);      //  Color table id: -1 = no table.

            AddDescriptorBox();
        }
    }
}
