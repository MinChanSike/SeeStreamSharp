﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    internal sealed class StreamSinkFactoryFactory
    {
        #region Singleton factory

        private static readonly StreamSinkFactoryFactory _instance = new StreamSinkFactoryFactory();

        //  Explicit static constructor to tell C# compiler not to mark type as beforefieldinit.
        static StreamSinkFactoryFactory()
        {
        }

        internal static StreamSinkFactoryFactory Instance { get { return _instance; } }

        #endregion

        private readonly ILog _logger = LogManager.GetLogger<Recording>();
        private readonly Dictionary<string, Type> _map = new Dictionary<string, Type>();

        private StreamSinkFactoryFactory()
        {
            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                object[] attribList = type.GetCustomAttributes(typeof(MediaFormatNameAttribute), false);
                foreach (object item in attribList)
                {
                    MediaFormatNameAttribute attrib = item as MediaFormatNameAttribute;
                    _map.Add(attrib.Name, type);
                }
            }
        }

        public Type Create(ISessionDescriptionSetup streamSource, out ListDictionary streams)
        {
            //  Key = rtpMap, IStreamSinkFactory
            streams = new ListDictionary();
            Type container = null;

            foreach (SDP.Media media in streamSource.SessionDescription.MediaDescriptions.Where((media) => {
                return media.Transport.StartsWith(streamSource.SupportedTransportProtocol) &&
                       (media.MediaType == SessionDescription.MediaType.audio ||
                       media.MediaType == SessionDescription.MediaType.video);
            }))
            {
                foreach (RtpMapAttribute rtpMap in media.OfType<RtpMapAttribute>())
                {
                    bool add = true;

                    try
                    {
                        IStreamSinkFactory ssf = Activator
                            .CreateInstance(_map[rtpMap.EncodingName], rtpMap) as IStreamSinkFactory;

                        if (ssf != null)
                        {
                            if (container == null)
                                container = ssf.GetContainerType();
                            else
                            {
                                if (container != ssf.GetContainerType())
                                {
                                    _logger.WarnFormat("Incompatible container {0} for media type {1}",
                                        ssf.GetContainerType().Name, rtpMap.MediaParent.MediaType);
                                    add = false;
                                }
                            }

                            if (add)
                            {
                                streams.Add(rtpMap, ssf);
                                //  Only add the first successful stream of this media type.
                                break;
                            }
                        }
                    }
                    catch (KeyNotFoundException)
                    {
                        _logger.WarnFormat("Encoding '{0}' not implemented",
                            rtpMap.EncodingName);
                    }
                    catch (Exception ex)
                    {
                        _logger.WarnFormat("Failed to create media sink, encoding name={1}",
                            ex, rtpMap.EncodingName);
                    }
                }
            }

            return container;
        }
    }
}
