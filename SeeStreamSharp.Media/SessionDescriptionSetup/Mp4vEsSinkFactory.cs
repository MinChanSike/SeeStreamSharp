﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.IsoBasedContainer;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// MP4V-ES SDP media description to stream sink factory converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 3016 RTP Payload Format for
    /// MPEG-4 Audio/Visual Streams (https://tools.ietf.org/html/rfc3016).
    /// </remarks>
    [MediaFormatName("MP4V-ES")]
    internal sealed class Mp4vEsSinkFactory : IStreamSinkFactory
    {
        private const string config = "config=";

        private readonly RtpMapAttribute _rtpMap;
        private readonly byte[] _config;

        public Mp4vEsSinkFactory(RtpMapAttribute rtpMap)
        {
            if (rtpMap == null)
                throw new ArgumentNullException("rtpMap");

            _rtpMap = rtpMap;

            //  There should only be one FmtpAttribute item that has the same PayloadType
            //  as the RtpMapAttribute.
            FmtpAttribute fmtp = rtpMap.MediaParent.OfType<FmtpAttribute>()
                .Where(o => { return o.PayloadType == rtpMap.PayloadType; }).FirstOrDefault();
            if (fmtp == null)
                throw new ArgumentException("No associated Fmtp attribute", "rtpMap");

            string[] parts = fmtp.FormatParameters.Split(';');
            foreach (string item in parts)
            {
                int param = item.IndexOf(config, StringComparison.Ordinal);
                if (param != -1)
                {
                    SoapHexBinary shb = SoapHexBinary.Parse(item.Substring(param + config.Length));
                    if (shb != null)
                        _config = shb.Value;
                }
            }

            if (_config == null)
                throw new ArgumentException("MPEG-4 config omitted in fmtp attribute parameters");
        }

        public void ConnectStreamSink(object container, Session session)
        {
            Mp4vEsFrameBuffer frameBuffer = new Mp4vEsFrameBuffer();
            Mp4vTrackWriter track = new Mp4vTrackWriter(container, _rtpMap.ClockRate,
                _config, frameBuffer);
            frameBuffer.Connect(track, session);
        }

        public Type GetContainerType()
        {
            return typeof(ContainerWriter);
        }
    }
}
