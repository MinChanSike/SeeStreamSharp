﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.IsoBasedContainer;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// H.264 SDP media description to stream sink factory converter.
    /// </summary>
    /// <remarks>
    /// Processes SDP media attributes as documented in RFC 3984 RTP Payload Format for
    /// H.264 Video (https://tools.ietf.org/html/rfc3984).
    /// </remarks>
    [MediaFormatName("H264")]
    internal sealed class H264SinkFactory : IStreamSinkFactory
    {
        private const string packetizationMode = "packetization-mode=";
        private const string profileLevelId = "profile-level-id=";
        private const string spps = "sprop-parameter-sets=";

        //  Default clock rate according to RFC 3984 is 90000.
        private readonly RtpMapAttribute _rtpMap;

        //  This code only supports Packetization Mode 0 and 1.
        private readonly int _packetMode = 1;
        private readonly byte[] _profileLevel = null;
        private readonly List<byte[]> _parameterSets = new List<byte[]>();

        /// Initializes a converter with the provided <see cref="RtpMapAttribute"/> source object.
        /// <param name="rtpMap"></param>
        public H264SinkFactory(RtpMapAttribute rtpMap)
        {
            if (rtpMap == null)
                throw new ArgumentNullException("rtpMap");

            _rtpMap = rtpMap;

            //  There should only be one FmtpAttribute item that has the same PayloadType
            //  as the RtpMapAttribute.
            FmtpAttribute fmtp = rtpMap.MediaParent.OfType<FmtpAttribute>()
                .Where(o => { return o.PayloadType == rtpMap.PayloadType; }).FirstOrDefault();
            if (fmtp == null)
                throw new ArgumentException("No associated Fmtp attribute", "rtpMap");

            string[] parts = fmtp.FormatParameters.Split(';');
            foreach (string item in parts)
            {
                int param = item.IndexOf(packetizationMode, StringComparison.Ordinal);
                if (param != -1)
                {
                    if (int.TryParse(item.Substring(param + packetizationMode.Length), out _packetMode))
                    {
                        if (_packetMode > 1 || _packetMode < 0)
                            throw new ArgumentException("Packetization mode not supported", "rtpMap");
                    }
                }

                param = item.IndexOf(profileLevelId, StringComparison.Ordinal);
                if (param != -1)
                {
                    SoapHexBinary shb = SoapHexBinary.Parse(item.Substring(param + profileLevelId.Length));
                    if (shb != null && shb.Value.Length == 3)
                        _profileLevel = shb.Value;
                }

                param = item.IndexOf(spps, StringComparison.Ordinal);
                if (param != -1)
                {
                    string base64 = item.Substring(param + spps.Length);
                    string[] props = base64.Split(',');

                    foreach (string prop in props)
                    {
                        //  May throw an exception if prop is malformed.
                        byte[] data = Convert.FromBase64String(prop);
                        BitVector32 nal = new BitVector32(data[0]);
                        int nalType = nal[H264FrameBuffer.s_nalType];
                        if (nalType == H264FrameBuffer.NAL_SPS || nalType == H264FrameBuffer.NAL_PPS)
                            _parameterSets.Add(data);
                    }
                }
            }
        }

        public void ConnectStreamSink(object container, Session session)
        {
            H264FrameBuffer frameBuffer = new H264FrameBuffer();
            Avc1TrackWriter track = new Avc1TrackWriter(container, _rtpMap.ClockRate,
                _profileLevel, _parameterSets,
                frameBuffer);
            frameBuffer.Connect(track, session);
        }

        public Type GetContainerType()
        {
            return typeof(ContainerWriter);
        }
    }
}
