﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.IsoBasedContainer;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// Mpeg4-Generic SDP media description to stream sink factory converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 3640 RTP Payload Format for
    /// Transport of MPEG-4 Elementary Streams (https://tools.ietf.org/html/rfc3640).
    /// </remarks>
    [MediaFormatName("mpeg4-generic")]
    internal sealed class Mpeg4GenericSinkFactory : IStreamSinkFactory
    {
        public enum MpegModes
        {
            Unknown,
            GENERIC,
            CELP_CBR,
            CELP_VBR,
            AAC_LBR,
            AAC_HBR
        }

        private const string mode = "mode=";
        private const string generic = "generic";
        private const string celp_cbr = "CELP-cbr";
        private const string celp_vbr = "CELP-vbr";
        private const string aac_lbr = "AAC-lbr";
        private const string aac_hbr = "AAC-hbr";

        private const string streamType = "streamType=";
        private const string profileLevelId = "profile-level-id=";
        private const string config = "config=";
        private const string sizelength = "sizelength=";
        private const string indexlength = "indexlength=";
        private const string indexdeltalength = "indexdeltalength=";
        private const string constantDuration = "constantDuration=";
        private const string maxDisplacement = "maxDisplacement=";
        private const string auxiliaryDataSizeLength = "auxiliaryDataSizeLength=";

        private const string fmtpNotImplementedFormat = "fmtp parameter '{0}' is not implemented";
        private const string fmtpInvalidValidFormat = "fmtp parameter '{0}' has invalid value";
        private const string fmtpParameterMissingFormat = "fmtp parameter '{0}' is missing";

        private readonly RtpMapAttribute _rtpMap;
        private readonly byte[] _config;
        private readonly MpegModes _mode;
        private readonly int _streamType, _profileLevelId,
            //  These may not be set in Fmtp but actually has fixed values in certain modes.
            _sizeLength = -1, _indexLenght = -1, _indexDeltaLength = -1,
            _constantDuration;

        public Mpeg4GenericSinkFactory(RtpMapAttribute rtpMap)
        {
            if (rtpMap == null)
                throw new ArgumentNullException("rtpMap");

            _rtpMap = rtpMap;

            //  There should only be one FmtpAttribute item that has the same PayloadType
            //  as the RtpMapAttribute.
            FmtpAttribute fmtp = rtpMap.MediaParent.OfType<FmtpAttribute>()
                .Where(o => { return o.PayloadType == rtpMap.PayloadType; }).FirstOrDefault();
            if (fmtp == null)
                throw new ArgumentException("No associated Fmtp attribute", "rtpMap");

            string[] parts = fmtp.FormatParameters.Split(';');
            foreach (string item in parts)
            {
                int param = item.IndexOf(streamType, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + streamType.Length), out _streamType);

                param = item.IndexOf(profileLevelId, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + profileLevelId.Length), out _profileLevelId);

                param = item.IndexOf(config, StringComparison.Ordinal);
                if (param != -1)
                {
                    SoapHexBinary shb = SoapHexBinary.Parse(item.Substring(param + config.Length));
                    if (shb != null)
                        _config = shb.Value;
                }

                param = item.IndexOf(mode, StringComparison.Ordinal);
                if (param != -1)
                {
                    if (item.Substring(param + mode.Length).StartsWith(generic, StringComparison.Ordinal))
                        _mode = MpegModes.GENERIC;
                    if (item.Substring(param + mode.Length).StartsWith(celp_cbr, StringComparison.Ordinal))
                        _mode = MpegModes.CELP_CBR;
                    if (item.Substring(param + mode.Length).StartsWith(celp_vbr, StringComparison.Ordinal))
                        _mode = MpegModes.CELP_VBR;
                    if (item.Substring(param + mode.Length).StartsWith(aac_lbr, StringComparison.Ordinal))
                        _mode = MpegModes.AAC_LBR;
                    if (item.Substring(param + mode.Length).StartsWith(aac_hbr, StringComparison.Ordinal))
                        _mode = MpegModes.AAC_HBR;
                }

                param = item.IndexOf(sizelength, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + sizelength.Length), out _sizeLength);

                param = item.IndexOf(indexlength, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + indexlength.Length), out _indexLenght);

                param = item.IndexOf(indexdeltalength, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + indexdeltalength.Length), out _indexDeltaLength);

                param = item.IndexOf(constantDuration, StringComparison.Ordinal);
                if (param != -1)
                    int.TryParse(item.Substring(param + constantDuration.Length), out _constantDuration);

                param = item.IndexOf(maxDisplacement, StringComparison.Ordinal);
                if (param != -1)
                    throw new ArgumentException(string.Format(fmtpNotImplementedFormat, maxDisplacement));

                param = item.IndexOf(auxiliaryDataSizeLength, StringComparison.Ordinal);
                if (param != -1)
                    throw new ArgumentException(string.Format(fmtpNotImplementedFormat, auxiliaryDataSizeLength));
            }

            if (_rtpMap.MediaParent.MediaType == SessionDescription.MediaType.audio)
            {
                switch (_mode)
                {
                    case MpegModes.Unknown:
                        throw new ArgumentException(string.Format(fmtpParameterMissingFormat, mode));

                    //case MpegModes.GENERIC:
                    //case MpegModes.CELP_CBR:
                    //case MpegModes.CELP_VBR:
                    //case MpegModes.AAC_LBR:
                    default:
                        throw new ArgumentException(string.Format(fmtpNotImplementedFormat, mode));

                    case MpegModes.AAC_HBR:
                        //  Assume correct value if not actually set in Fmtp.
                        if (_sizeLength == -1)
                            _sizeLength = 13;
                        if (_sizeLength != 13)
                            throw new ArgumentException(string.Format(fmtpInvalidValidFormat, sizelength));
                        //  Assume correct value if not actually set in Fmtp.
                        if (_indexLenght == -1)
                            _indexLenght = 3;
                        if (_indexLenght != 3)
                            throw new ArgumentException(string.Format(fmtpInvalidValidFormat, indexlength));
                        //  Assume correct value if not actually set in Fmtp.
                        if (_indexDeltaLength == -1)
                            _indexDeltaLength = 3;
                        if (_indexDeltaLength != 3)
                            throw new ArgumentException(string.Format(fmtpInvalidValidFormat, indexdeltalength));
                        break;
                }

                if (_streamType == 0)
                    _streamType = Mpeg4.AUDIO_ISO_14496_3;
            }
            else if (_rtpMap.MediaParent.MediaType == SessionDescription.MediaType.video)
            {
                if (_mode != MpegModes.GENERIC)
                    throw new ArgumentException(string.Format(fmtpNotImplementedFormat, mode));

                if (_streamType == 0)
                    _streamType = Mpeg4.VISUAL_ISO_14496_2;
            }
            else
                throw new ArgumentException("SDP media type not implemented");
        }

        public void ConnectStreamSink(object container, Session session)
        {
            if (_mode == MpegModes.AAC_HBR)
            {
                Mpeg4AacHbrFrameBuffer frameBuffer = new Mpeg4AacHbrFrameBuffer(_constantDuration);
                Mp4aTrackWriter audio = new Mp4aTrackWriter(container, _rtpMap.ClockRate,
                    _config, frameBuffer);
                frameBuffer.Connect(audio, session);
            }
        }

        public Type GetContainerType()
        {
            return typeof(ContainerWriter);
        }
    }
}
