﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeeStreamSharp.SDP.Tests
{
    [TestClass()]
    public class SessionDescriptionTests
    {
        private static void Compare(string input, StringBuilder sb)
        {
            string result = sb.ToString();
            Debug.WriteLine(input);
            Debug.WriteLine(result);

            bool equals = false;
            int il = input.Length, rl = result.Length;
            Debug.WriteLine(string.Format("input.Length = {0}, result.Length = {1}", il, rl));
            if (il > rl)
            {
                equals = input.StartsWith(result, StringComparison.Ordinal);
            }
            else
            {
                equals = result.StartsWith(input, StringComparison.Ordinal);
            }
            Assert.IsTrue(equals, "Result is not equal to input");
        }

        [TestMethod()]
        public void ParseTest1()
        {
            string input = @"v=0
o=mhandley 2890844526 2890842807 IN IP4 126.16.64.4
s=SDP Seminar
i=A Seminar on the session description protocol
u=http://www.cs.ucl.ac.uk/staff/M.Handley/sdp.03.ps
e=mjh@isi.edu (Mark Handley)
c=IN IP4 224.2.17.12/127
t=2873397496 2873404696
a=recvonly
m=audio 49170 RTP/AVP 0
m=video 51372 RTP/AVP 31
m=application 32416 udp wb
a=orient:portrait
";

            SessionDescription sd = SessionDescription.Parse(input);

            StringBuilder sb = new StringBuilder(input.Length + 10);
            int i = 0;
            foreach (TypeValue tv in sd)
            {
                string line = tv.ToString();
                sb.AppendLine(line);
                i++;
            }

            Assert.AreEqual<int>(13, i, "Invalid number of SdpTypeValue objects");

            Compare(input, sb);
        }

        [TestMethod()]
        public void ParseTest2()
        {
            string input = @"v=0
o=- 1218949574973387 1218949574973400 IN IP4 192.168.70.252
s=Media Presentation
e=NONE
c=IN IP4 0.0.0.0
b=AS:8000
t=0 0
a=control:*
a=range:npt=now-
a=mpeg4-iod: " +
"\"data:application/mpeg4-iod;base64,AoDUAE8BAf/1AQOAbwABQFBkYXRhOmFwcGxpY2F0aW9uL21wZWc0LW9kLWF1O2Jhc2U2NCxBUjBCR3dVZkF4Y0F5U1FBWlFRTklCRUVrK0FBZWhJQUFIb1NBQVlCQkE9PQQNAQUABAAAAAAAAAAAAAYJAQAAAAAAAAAAAzoAAkA2ZGF0YTphcHBsaWNhdGlvbi9tcGVnNC1iaWZzLWF1O2Jhc2U2NCx3QkFTWVFTSVVFVUZQd0E9BBICDQAAAgAAAAAAAAAABQMAAEAGCQEAAAAAAAAAAA==\"" +
@"
m=video 0 RTP/AVP 96
b=AS:8000
a=framerate:30.0
a=control:trackID=1
a=rtpmap:96 MP4V-ES/90000
a=fmtp:96 profile-level-id=245; config=000001B0F5000001B509000001000000012008D48D88032514043C14440F
a=mpeg4-esid:201
";

            SessionDescription sd = SessionDescription.Parse(input);

            StringBuilder sb = new StringBuilder(input.Length + 10);
            int i = 0;
            foreach (TypeValue tv in sd)
            {
                string line = tv.ToString();
                sb.AppendLine(line);
                i++;
            }

            Assert.AreEqual<int>(17, i, "Invalid number of SdpTypeValue objects");

            Compare(input, sb);
        }

        [TestMethod()]
        public void ParseTest3()
        {
            string input = @"v=0
o=- 1418374838887877 1418374838887877 IN IP4 192.168.1.55
s=Media Presentation
e=NONE
c=IN IP4 0.0.0.0
b=AS:50000
t=0 0
a=control:rtsp://192.168.1.55/axis-media/media.amp?fps=20&resolution=800x600
a=range:npt=0.000000-
m=video 0 RTP/AVP 96
b=AS:50000
a=framerate:20.0
a=control:rtsp://192.168.1.55/axis-media/media.amp/trackID=1?fps=20&resolution=800x600
a=rtpmap:96 H264/90000
a=fmtp:96 packetization-mode=1; profile-level-id=420029; sprop-parameter-sets=Z0IAKeKQGQJvy4C3AQEBpB4kRUA=,aM48gA==
";

            SessionDescription sd = SessionDescription.Parse(input);

            StringBuilder sb = new StringBuilder(input.Length + 10);
            int i = 0;
            foreach (TypeValue tv in sd)
            {
                string line = tv.ToString();
                sb.AppendLine(line);
                i++;
            }

            Assert.AreEqual<int>(15, i, "Invalid number of SdpTypeValue objects");

            Compare(input, sb);
        }

        [TestMethod]
        public void SdpParseMultimedia()
        {
            string input = @"v=0
o=- 1649242139 1649242139 IN IP4 184.72.239.149
s=BigBuckBunny_115k.mov
c=IN IP4 184.72.239.149
t=0 0
a=sdplang:en
a=range:npt=0- 596.48
a=control:*
m=audio 0 RTP/AVP 96
a=rtpmap:96 mpeg4-generic/12000/2
a=fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1490
a=control:trackID=1
m=video 0 RTP/AVP 97
a=rtpmap:97 H264/90000
a=fmtp:97 packetization-mode=1;profile-level-id=42C01E;sprop-parameter-sets=Z0LAHtkDxWhAAAADAEAAAAwDxYuS,aMuMsg==
a=cliprect:0,0,160,240
a=framesize:97 240-160
a=framerate:24.0
a=control:trackID=2
";

            SessionDescription sd = SessionDescription.Parse(input);

            StringBuilder sb = new StringBuilder(input.Length + 10);
            int i = 0;
            foreach (TypeValue tv in sd)
            {
                string line = tv.ToString();
                sb.AppendLine(line);
                i++;
            }

            Assert.AreEqual<int>(19, i, "Invalid number of SdpTypeValue objects");

            Compare(input, sb);
        }
    }
}