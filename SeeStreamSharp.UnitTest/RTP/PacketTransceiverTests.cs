﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace SeeStreamSharp.RTP.Tests
{
    [TestClass()]
    public sealed class PacketTransceiverTests : IObserver<Packet>
    {
        [TestMethod()]
        public void PacketTransceiverTest()
        {
            PacketTransceiver pt = new PacketTransceiver(this);

            byte[] array = new byte[] { 0x80, 0xE0, 0xF9, 0xFF, 0x74, 0xFA, 0x47, 0x51, 0x81, 0xE4, 0x40, 0xBC };
            ArraySegment<byte> data = new ArraySegment<byte>(array);

            pt.OnNext(data);
        }

        void IObserver<Packet>.OnCompleted()
        {
            throw new NotImplementedException();
        }

        void IObserver<Packet>.OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        void IObserver<Packet>.OnNext(Packet packet)
        {
            Assert.AreEqual(63999, packet.Header.Sequence); //  0xf9ff

            Assert.AreEqual(2, packet.Header.Version);
            Assert.IsFalse(packet.Header.Padding);
            Assert.IsFalse(packet.Header.Extension);
            Assert.AreEqual(0, packet.Header.CsrcCount);
            Assert.IsTrue(packet.Header.Marker);
            Assert.AreEqual(96, packet.Header.PayloadType);    //  dynamic

            long l = (uint)packet.Header.TimeStamp;
            Assert.AreEqual(0x74FA4751L, l, "TimeStamp");

            l = (uint)packet.Header.Ssrc;
            Assert.AreEqual(0x81E440BCL, l, "SSRC");
        }
    }
}