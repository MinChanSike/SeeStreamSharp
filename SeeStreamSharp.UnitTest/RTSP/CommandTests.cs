﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net;

namespace SeeStreamSharp.RTSP.Tests
{
    [TestClass()]
    public class CommandTests
    {
        [TestMethod()]
        public void CreateDescribe1Test()
        {
            string result = "DESCRIBE rtsp://192.168.1.55/axis-media/media.amp RTSP/1.0\r\n" +
                            "Accept: application/sdp\r\n\r\n";

            Uri presentation = new Uri("rtsp://192.168.1.55/axis-media/media.amp", UriKind.Absolute);
            Describe setup = new Describe(presentation, null, 0);
            Debug.Write(setup.ToString());
            Assert.AreEqual(result, setup.ToString());
        }

        [TestMethod]
        public void CreateSetup1Test()
        {
            string result = "SETUP rtsp://192.168.1.55/axis-media/media.amp/trackID=1?fps=20&resolution=800x600 RTSP/1.0\r\n" +
                            "Transport: RTP/AVP;unicast;client_port=4588-4589\r\n\r\n";

            Uri url = new Uri("rtsp://192.168.1.55/axis-media/media.amp/trackID=1?fps=20&resolution=800x600", UriKind.Absolute);

            //  This presentation would fail to get the session description. But this Url
            //  is only reused to ease the construction of a Presentation object that already
            //  implements an interface required to construct the tested Setup object.
            Presentation presentation = new Presentation(url);
            Setup setup = new Setup(url, null, 0);
            setup.SetUnicastUdpTransport(presentation.SupportedTransportProtocol, 4588);
            Debug.Write(setup.ToString());
            Assert.AreEqual(result, setup.ToString());

            int port;
            int.TryParse("4588", out port);
            Assert.AreEqual(4588, port);
        }

        [TestMethod]
        public void ParseHeaders1Test()
        {
            string result = "RTSP/1.0 200 OK\r\n" +
                            "CSeq: 302\r\n" +
                            "Date: 23 Jan 1997 15:35:06 GMT\r\n" +
                            "Session: 47112344\r\n" +
                            "Transport: RTP/AVP;unicast;client_port=4588-4589;server_port=6256-6257\r\n\r\n";
            string[] input = new string[] { "CSeq: 302",
                                            "Date: 23 Jan 1997 15:35:06 GMT",
                                            "Session: 47112344",
                                            "Transport: RTP/AVP;unicast;",
                                            "    client_port=4588-4589;server_port=6256-6257" };

            Response response = new Response(HttpStatusCode.OK, "OK");
            foreach (string line in input)
            {
                response.Headers.ParseLine(line);
            }

            //Debug.Write(response.ToString());
            Assert.AreEqual(result, response.ToString());
        }

        [TestMethod]
        public void ParseHeaders2Test()
        {
            string result = "RTSP/1.0 200 OK\r\n" +
                            "CSeq: 3\r\n" +
                            "Session: 2034820394\r\n" +
                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;rtptime=3781123\r\n\r\n";
            string[] input = new string[] { "CSeq: 3",
                                            "Session: 2034820394",
                                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;",
                                            "\tseq=981888;rtptime=3781123" };

            Response response = new Response(HttpStatusCode.OK, "OK");
            foreach (string line in input)
            {
                response.Headers.ParseLine(line);
            }

            //Debug.Write(response.ToString());
            Assert.AreEqual(result, response.ToString());
        }

        [TestMethod]
        public void ParseHeaders3Test()
        {
            string result = "RTSP/1.0 200 OK\r\n" +
                            "CSeq: 3\r\n" +
                            "Session: 2034820394\r\n" +
                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;rtptime=3781123\r\n\r\n";
            string[] input = new string[] { "CSeq: 3",
                                            "Session: 2034820394",
                                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;",
                                            "\trtptime=3781123" };

            Response response = new Response(HttpStatusCode.OK, "OK");
            foreach (string line in input)
            {
                response.Headers.ParseLine(line);
            }

            //Debug.Write(response.ToString());
            Assert.AreEqual(result, response.ToString());
        }

        [TestMethod]
        public void ParseHeaders4Test()
        {
            string result = "RTSP/1.0 200 OK\r\n" +
                            "CSeq: 3\r\n" +
                            "Session: 2034820394\r\n" +
                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;rtptime=3781123," +
                            "url=rtsp://foo.com/test.wav/streamid=1;seq=4321;rtptime=3781125\r\n\r\n";
            string[] input = new string[] { "CSeq: 3",
                                            "Session: 2034820394",
                                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;",
                                            "\trtptime=3781123,url=rtsp://foo.com/test.wav/streamid=1;",
                                            "\tseq=4321;rtptime=3781125" };

            Response response = new Response(HttpStatusCode.OK, "OK");
            foreach (string line in input)
            {
                response.Headers.ParseLine(line);
            }

            //Debug.Write(response.ToString());
            Assert.AreEqual(result, response.ToString());
        }

        [TestMethod]
        public void ParseHeaders5Test()
        {
            string result = "RTSP/1.0 200 OK\r\n" +
                            "CSeq: 3\r\n" +
                            "Session: 2034820394\r\n" +
                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;rtptime=3781123," +
                            "url=rtsp://foo.com/test.wav/streamid=1;seq=4321;rtptime=3781125\r\n\r\n";
            string[] input = new string[] { "CSeq: 3",
                                            "Session: 2034820394",
                                            "RTP-Info: url=rtsp://foo.com/test.wav/streamid=0;seq=981888;rtptime=3781123,",
                                            "    url=rtsp://foo.com/test.wav/streamid=1;seq=4321;",
                                            "    rtptime=3781125" };

            Response response = new Response(HttpStatusCode.OK, "OK");
            foreach (string line in input)
            {
                response.Headers.ParseLine(line);
            }

            //Debug.Write(response.ToString());
            Assert.AreEqual(result, response.ToString());
        }
    }
}
